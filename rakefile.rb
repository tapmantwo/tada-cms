# Build version
load "VERSION.txt"

# Build information
SOLUTION_NAME = "Tada CMS"
SOLUTION_DESC = "Tada.CMS."
SOLUTION_LICENSE = "http://nuget.dev.ahc/license"
SOLUTION_URL = "http://tada.com"
SOLUTION_COMPANY = "Touchcap"
SOLUTION_COPYRIGHT = "Copyright (c) #{SOLUTION_COMPANY} 2014"

# Build configuration
SOLUTION_FILE = "tada.cms.sln"
SERVICE_NAME = "cms"
ROOT_NAMESPACE = "tada.#{SERVICE_NAME}."
CONFIG = ENV["config"] || "Release"
PLATFORM = ENV["platform"] || "Any CPU"
WEB_PLATFORM = "Any CPU"

# Test configuration
KARMA_CONFIG_UNIT = nil
KARMA_E2E_PORT = nil
KARMA_CONFIG_E2E = nil

ENV["EnableNuGetPackageRestore"] = "true"

# Lets import our common rake process
toolsfiles = Dir["#{File.dirname(__FILE__)}/tools/rake/*.rb"]

if( ! toolsfiles.any? )
  # no tools, let's go get it for them.
  sh 'git submodule update --init' unless toolsfiles.any?
  toolsfiles = Dir["#{File.dirname(__FILE__)}/tools/rake/*.rb"]
end

# nope, we still don't have tools. Something went wrong.
raise "Run `git submodule update --init` to populate your tools folder." unless toolsfiles.any?

toolsfiles.each { |ext| load ext }