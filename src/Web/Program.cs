﻿using System.Reflection;

namespace Tada.Cms.Web
{
    using System;
    using System.Configuration;
    using System.Diagnostics;
    using System.IO;
    using System.Threading.Tasks;
    using System.Web.Cors;

    using Microsoft.Owin;
    using Microsoft.Owin.Cors;
    using Microsoft.Owin.Diagnostics;
    using Microsoft.Owin.FileSystems;
    using Microsoft.Owin.Hosting;
    using Microsoft.Owin.StaticFiles;

    using Owin;

    using Topshelf;

    internal class Program
    {
        public static readonly string[] PublicFolders = new[] { "/styling", "/components", "/assets", "/diagnostics" };

        private static readonly Type[] EnforceReferencesFor =
            {
                typeof(Microsoft.Owin.Host.HttpListener.OwinServerFactory),
            };

        private static readonly Func<StartOptions, IDisposable> Host = options => WebApp.Start(
            options,
            appBuilder =>
            {
                appBuilder.UseErrorPage(
                                    new ErrorPageOptions
                                    {
                                        ShowCookies = true,
                                        ShowEnvironment = true,
                                        ShowExceptionDetails = true,
                                        ShowHeaders = true,
                                        ShowQuery = true,
                                        ShowSourceCode = true,
                                        SourceCodeLineCount = 100
                                    });

                appBuilder.UseCors(new CorsOptions
                {
                    PolicyProvider = new CorsPolicyProvider
                    {
                        PolicyResolver = context => Task.FromResult(new CorsPolicy
                        {
                            AllowAnyHeader = true,
                            AllowAnyMethod = true,
                            AllowAnyOrigin = true,
                            SupportsCredentials = false
                        })
                    }
                });

                var rootDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                if (rootDir != null)
                {
                    var appDir = Path.Combine(rootDir, "app");

                    if (Directory.Exists(appDir))
                    {
                        appBuilder.UseDefaultFiles(new DefaultFilesOptions
                            {
                                DefaultFileNames = new[] {"index.html"},
                                RequestPath = new PathString("/contentmanagement"),
                                FileSystem = new PhysicalFileSystem(appDir)
                            });

                        appBuilder.UseStaticFiles(
                            new StaticFileOptions
                                {
                                    RequestPath = new PathString("/contentmanagement"),
                                    FileSystem = new PhysicalFileSystem(appDir)
                                });
                    }

                    foreach (var folder in PublicFolders)
                    {
                        var publicFolderPath = Path.Combine(rootDir, folder.Trim('\\', '/'));
                        if (Directory.Exists(publicFolderPath))
                        {
                            appBuilder.UseStaticFiles(
                                new StaticFileOptions
                                    {
                                        RequestPath = new PathString("/contentmanagement" + folder),
                                        FileSystem = new PhysicalFileSystem(publicFolderPath)
                                    });
                        }
                    }
                }
            });

        public static IDisposable LaunchTestServer(string server = "localhost", int port = 55000)
        {
            return Host(new StartOptions(string.Format("http://{0}:{1}", server, port)));
        }

        private static void Main(string[] args)
        {
            string hostname = ConfigurationManager.AppSettings["server"] ?? "localhost";

            int port;

            if (!int.TryParse(ConfigurationManager.AppSettings["port"], out port))
            {
                throw new Exception("Couldn't find port setting in appSettings.config.");
            }

            HostFactory.Run(
                ts =>
                {
                    ts.AddCommandLineDefinition(
                        "port",
                        s =>
                        {
                            int argPort;

                            if (int.TryParse(s, out argPort))
                            {
                                port = argPort;
                            }
                        });

                    ts.Service<StartOptions>(
                        s =>
                        {
                            IDisposable owinHost = null;

                            s.ConstructUsing(
                                settings => new StartOptions(string.Format("http://{0}:{1}", hostname, port)));

                            s.WhenStarted(
                                options =>
                                {
                                    owinHost = Host(options);

                                    Trace.WriteLine(
                                        string.Format("Listening on http://{0}:{1}", hostname, port));
                                });

                            s.WhenStopped(
                                callback =>
                                {
                                    if (owinHost != null)
                                    {
                                        owinHost.Dispose();
                                    }
                                });
                        });

                    ts.RunAsLocalSystem();
                    ts.SetDisplayName("[Tada Cms] Web Content Management");
                    ts.SetServiceName("tada-cms-web-content-management");
                });
        }
    }
}