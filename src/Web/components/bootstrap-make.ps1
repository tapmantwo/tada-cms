New-Item bootstrap\build -type directory -force
New-Item bootstrap\build\css -type directory -force
New-Item bootstrap\build\fonts -type directory -force
New-Item bootstrap\build\js -type directory -force

Copy-Item bootstrap\fonts\* bootstrap\build\fonts -force

&..\..\..\tools\node_modules\.bin\lessc bootstrap\less\bootstrap.less > bootstrap\build\css\bootstrap.css
&..\..\..\tools\node_modules\.bin\lessc -x bootstrap\less\bootstrap.less > bootstrap\build\css\bootstrap.min.css

&..\..\..\tools\node_modules\.bin\lessc bootstrap\less\theme.less > bootstrap\build\css\bootstrap-theme.css
&..\..\..\tools\node_modules\.bin\lessc -x bootstrap\less\theme.less > bootstrap\build\css\bootstrap-theme.min.css

# make sure bootstrap-tooltip.js appears before bootstrap-popover.js - https://github.com/twbs/bootstrap/issues/2539
gc bootstrap\js\affix.js,bootstrap\js\alert.js,bootstrap\js\button.js,bootstrap\js\carousel.js,bootstrap\js\collapse.js,bootstrap\js\dropdown.js,bootstrap\js\modal.js,bootstrap\js\scrollspy.js,bootstrap\js\tab.js,bootstrap\js\tooltip.js,bootstrap\js\popover.js,bootstrap\js\transition.js -Enc Byte | sc bootstrap\build\js\bootstrap.js -Enc Byte

&..\..\..\tools\node_modules\.bin\uglifyjs bootstrap\build\js\bootstrap.js > bootstrap\build\js\bootstrap.min.js