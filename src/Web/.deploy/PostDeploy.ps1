# [CmdletBinding()]
# Param 
# (
#     [string]$AppointmentsQueryHostScheme,
#     [string]$AppointmentsQueryHostName,
#     [string]$AppointmentsQueryHostPort,
#     [string]$AppointmentsQueryHostPath,
#     [string]$AppointmentsCommandHostScheme,
#     [string]$AppointmentsCommandHostName,
#     [string]$AppointmentsCommandHostPort,
#     [string]$AppointmentsCommandHostPath,
#     [string]$ProvisioningQueryHostScheme,
#     [string]$ProvisioningQueryHostName,
#     [string]$ProvisioningQueryHostPort,
#     [string]$ProvisioningQueryHostPath,
#     [string]$PersonnelQueryHostScheme,
#     [string]$PersonnelQueryHostName,
#     [string]$PersonnelQueryHostPort,
#     [string]$PersonnelQueryHostPath,
#     [string]$PatientQueryHostScheme,
#     [string]$PatientQueryHostName,
#     [string]$PatientQueryHostPort,
#     [string]$PatientQueryHostPath
#     [string]$WebRootPath
# )

# These variables should be set via the Octopus web portal:
#
#   AppointmentsQueryHostScheme   - The URI scheme for querying appointments service (e.g. http)
#   AppointmentsQueryHostName     - The hostname for querying appointments service (e.g. localhost)
#   AppointmentsQueryHostPort     - The port for querying appointments service (e.g. 80)
#   AppointmentsQueryHostPath     - The path for querying appointments service (e.g. '' or '/_appointments-query')
#   AppointmentsCommandHostScheme - The URI scheme for sending commands to the appointments service (e.g. http)
#   AppointmentsCommandHostName   - The hostname for sending commands to the appointments service (e.g. localhost)
#   AppointmentsCommandHostPort   - The port for sending commands to the appointments service (e.g. 80)
#   AppointmentsCommandHostPath   - The path for querying clinial service (e.g. '' or '/_appointments-command')
#   ProvisioningQueryHostScheme   - The URI scheme for querying provisioning service (e.g. http)
#   ProvisioningQueryHostName     - The hostname for querying provisioning service (e.g. localhost)
#   ProvisioningQueryHostPort     - The port for querying provisioning service (e.g. 80)
#   ProvisioningQueryHostPath     - The path for querying provisioning service (e.g. '' or '/_provisioning-query')
#   PersonnelQueryHostScheme      - The URI scheme for querying personnel service (e.g. http)
#   PersonnelQueryHostName        - The hostname for querying personnel service (e.g. localhost)
#   PersonnelQueryHostPort        - The port for querying personnel service (e.g. 80)
#   PersonnelQueryHostPath        - The path for querying personnel service (e.g. '' or '/_personnel-query')
#   PatientQueryHostScheme        - The URI scheme for querying patient service (e.g. http)
#   PatientQueryHostName          - The hostname for querying patient service (e.g. localhost)
#   PatientQueryHostPort          - The port for querying patient service (e.g. 80)
#   PatientQueryHostPath          - The path for querying patient service (e.g. '' or '/_patient-query')
#   WebRootPath                   - The (physical) path to the root of the website

Function ParseHostPath
{
	Param ([string]$hostPath)

	If ([string]::IsNullOrWhiteSpace($hostPath.Trim("/")))
	{
		return ''
	}
	Else
	{
		return '/' + $hostPath.Trim("/")
	}
}

# Back up the original js file
$jsHostPath = Resolve-Path "${WebRootPath}\app\global\global.min.js"

Write-Host $jsHostPath

Copy-Item $jsHostPath ($jsHostPath.Path + '.original')

(Get-Content $jsHostPath.Path) | Foreach-Object {
    $_ = $_ -replace 'APPOINTMENT_QUERY_HOST(\s)?=(\s)?"([^"])+"', ('APPOINTMENT_QUERY_HOST="' + $AppointmentsQueryHostScheme + '://' + $AppointmentsQueryHostName + ':' + $AppointmentsQueryHostPort + $(ParseHostPath -hostPath $AppointmentsQueryHostPath) + '"')
    $_ = $_ -replace 'APPOINTMENT_COMMAND_HOST(\s)?=(\s)?"([^"])+"', ('APPOINTMENT_COMMAND_HOST="' + $AppointmentsCommandHostScheme + '://' + $AppointmentsCommandHostName + ':' + $AppointmentsCommandHostPort + $(ParseHostPath -hostPath $AppointmentsCommandHostPath) + '"')
    $_ = $_ -replace 'PROVISIONING_QUERY_HOST(\s)?=(\s)?"([^"])+"', ('PROVISIONING_QUERY_HOST="' + $ProvisioningQueryHostScheme + '://' + $ProvisioningQueryHostName + ':' + $ProvisioningQueryHostPort + $(ParseHostPath -hostPath $ProvisioningQueryHostPath) + '"')
    $_ = $_ -replace 'PERSONNEL_QUERY_HOST(\s)?=(\s)?"([^"])+"', ('PERSONNEL_QUERY_HOST="' + $PersonnelQueryHostScheme + '://' + $PersonnelQueryHostName + ':' + $PersonnelQueryHostPort + $(ParseHostPath -hostPath $PersonnelQueryHostPath) + '"')
    $_ = $_ -replace 'PATIENT_QUERY_HOST(\s)?=(\s)?"([^"])+"', ('PATIENT_QUERY_HOST="' + $PatientQueryHostScheme + '://' + $PatientQueryHostName + ':' + $PatientQueryHostPort + $(ParseHostPath -hostPath $PatientQueryHostPath) + '"')
    $_
} | Set-Content $jsHostPath.Path	