var tada = tada || {};
tada.articles = angular.module("articlesModule", ["global", "ngResource", "ngRoute", "ngCkeditor"])
    .config(["$routeProvider", function($routeProvider) {
        "use strict";
        $routeProvider.when("/list/:categoryId?", {
            templateUrl: "./templates/list.html",
            controller: "articleListController"
        });

        $routeProvider.when("/addCategory/:parentCategoryId", {
            templateUrl: "./templates/new-category.html",
            controller: "newCategoryController",
        });

        $routeProvider.when("/editCategory/:parentCategoryId/:categoryId", {
            templateUrl: "./templates/edit-category.html",
            controller: "editCategoryController",
        });

        $routeProvider.when("/addArticle/:categoryId", {
            templateUrl: "./templates/new-article.html",
            controller: "newArticleController",
        });

        $routeProvider.when("/editArticle/:categoryId/:articleId", {
            templateUrl: "./templates/edit-article.html",
            controller: "editArticleController",
        });

        $routeProvider.when("/deleteArticle/:categoryId/:articleId", {
            templateUrl: "./templates/delete-article.html",
            controller: "deleteArticleController",
        });

        $routeProvider.otherwise({ redirectTo : "/list" });
    }]);
