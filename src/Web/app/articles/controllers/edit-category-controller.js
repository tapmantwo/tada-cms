var tada = tada || {};
tada.articles = tada.articles || {};
tada.articles.controller("editCategoryController", ["$scope", "articleService", "$routeParams", "$location", function($scope, articleService, $routeParams, $location) {
    "use strict";

    $scope.errors = [];
    $scope.category = null;
    $scope.editCategoryId = $routeParams.categoryId;
    $scope.parentCategoryId = $routeParams.parentCategoryId || null;

    articleService.getCategory($scope.editCategoryId,
        function(result) {
            $scope.category = result;
        },
        function(err) {
            $scope.errors.push( "Could not load the category.");
        });

    $scope.editCategory = function() {
        // Validate the category name is unique for this parent
        $scope.errors = [];

        if (articleService.categoryWithNameExists($scope.parentCategoryId, $scope.category.articleCategoryName, $scope.editCategoryId)) {
            $scope.errors.push("The specified name is already in use, please choose another.");
            return false;
        }

        articleService.editCategory($scope.category,
            function() {
                $location.path("/list/" + $scope.category.articleCategoryId);
            },
            function(err) {
                $scope.errors.push( "Could not update the category.");
            });
    };
}]);

