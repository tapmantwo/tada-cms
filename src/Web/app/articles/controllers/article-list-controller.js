var tada = tada || {};
tada.articles = tada.articles || {};
tada.articles.controller("articleListController", ["$scope", "articleService", "$routeParams", "arrayUtils", "$location", function($scope, articleService, $routeParams, arrayUtils, $location) {
    "use strict";

    $scope.errors = [];
    $scope.currentCategory = null;
    $scope.categoryId = $routeParams.categoryId || null;
    $scope.articleList = null;
    $scope.showDeleted = false;
    $scope.pageList = null;
    $scope.previousCategories = [];
    $scope.previousCategory = null;

    articleService.getArticleList(
        function(result) {
            $scope.articleList = result;

            if ( $scope.categoryId ) {
                // Find the specified category
                var route = [];
                var category = findCategory($scope.categoryId, result, route);
                if ( category ) {
                    $scope.previousCategory = route[0];
                    $scope.previousCategories = arrayUtils.reverse(route);
                    $scope.currentCategory = category;
                } else {
                    $scope.previousCategories = [];
                    $scope.currentCategory = result[0];
                }
            } else {
                $scope.previousCategories = [];
                $scope.currentCategory = result[0];
            }

            $scope.categoryId = $scope.currentCategory.articleCategoryId;
        },
        function(err) {
            $scope.errors.push("Could not load articles.");
        });

    $scope.selectCategory = function(category)
    {
        if ( category.articleCategoryId ) {
            $location.path("/list/" + category.articleCategoryId);
        } else {
            $location.path("/list");
        }
    };

    function findCategory(categoryId, categories, route)
    {
        var category = null;

        var i;
        for( i = 0; i < categories.length; i++ ) {
            if ( categories[i].articleCategoryId === categoryId ) {
                category = categories[i];
                break;
            }
        }

        if ( !category ) {
            // Check subcategories instead then
            for( i = 0; i < categories.length; i++ ) {
                var subCategory = findCategory(categoryId, categories[i].categories, route);
                if ( subCategory ) {
                    category = subCategory;
                    route.push(categories[i]);
                    break;
                }
            }
        }

        return category;
    }
}]);

