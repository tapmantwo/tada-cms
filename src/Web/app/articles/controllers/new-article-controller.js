var tada = tada || {};
tada.articles = tada.articles || {};
tada.articles.controller("newArticleController", ["$scope", "articleService", "$routeParams", "$location", "arrayUtils", function($scope, articleService, $routeParams, $location, arrayUtils) {
    "use strict";

    $scope.errors = [];
    $scope.categoryId = $routeParams.categoryId || null;

    $scope.article = {
            title: null,
            summary: null,
            content: "",
        };

    // setup editor options
    $scope.editorOptions = {
        uiColor: "#000000"
    };

    $scope.addArticle = function() {
        $scope.errors = [];

        var okToSave = true;

        if ( !$scope.article.title || $scope.article.title.length === 0) {
            $scope.errors.push("Please enter a title.");
            return false;
        }

        if ( articleService.articleWithTitleExists($scope.categoryId, $scope.article.title, null)) {
            $scope.errors.push("The specified title is already in use in this category, please choose another.");
            return false;
        }

        articleService.addArticle($scope.categoryId, $scope.article,
            function() {
                $location.path("/list/" + $scope.categoryId);
            },
            function(err) {
                $scope.errors.push( "Could not update the page.");
            });
    };
}]);

