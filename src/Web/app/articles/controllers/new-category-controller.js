var tada = tada || {};
tada.articles = tada.articles || {};
tada.articles.controller("newCategoryController", ["$scope", "articleService", "$routeParams", "$location", function($scope, articleService, $routeParams, $location) {
    "use strict";

    $scope.errors = [];
    $scope.categoryName = "";
    $scope.parentCategoryId = $routeParams.parentCategoryId || null;

    $scope.addCategory = function() {
        // Validate the category name is unique for this parent
        $scope.errors = [];

        if ( articleService.categoryWithNameExists($scope.parentCategoryId, $scope.categoryName, null)) {
            $scope.errors.push("The specified name is already in use, please choose another.");
            return false;
        }

        // create
        var category =  {
            articleCategoryId: null,
            articleCategoryName: $scope.categoryName,
            categories: [],
            articles: []
        };

        articleService.addCategory($scope.parentCategoryId, category,
            function(id) {
                $location.path("/list/" + id);
            },
            function(err) {
                $scope.errors.push("Could not add the category.");
            });
    };
}]);

