var tada = tada || {};
tada.articles = tada.articles || {};
tada.articles.controller("deleteArticleController", ["$scope", "articleService", "$routeParams", "$location", function($scope, articleService, $routeParams, $location) {
    "use strict";

    $scope.errors = [];
    $scope.articleId = $routeParams.articleId || null;
    $scope.categoryId = $routeParams.categoryId || null;
    $scope.article = null;

    articleService.getArticle($scope.articleId,
        function(result) {
            $scope.article = result;
        },
        function(err) {
            $scope.errors.push("Could not load article.");
        });

    $scope.deleteArticle = function() {
        $scope.errors = [];

        articleService.deleteArticle($scope.categoryId, $scope.articleId,
            function(id) {
                $location.path("/list/" + $scope.categoryId);
            },
            function(err) {
                $scope.errors.push("Could not delete the article.");
            });
    };
}]);

