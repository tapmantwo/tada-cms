var tada = tada || {};
tada.articles = tada.articles || {};
tada.articles.controller("editArticleController", ["$scope", "articleService", "$routeParams", "$location", "arrayUtils", function($scope, articleService, $routeParams, $location, arrayUtils) {
    "use strict";

    $scope.errors = [];
    $scope.categoryId = $routeParams.categoryId || null;
    $scope.articleId = $routeParams.articleId || null;

    $scope.article = null;

    articleService.getArticle($scope.articleId,
        function(article) {
            // copy, so we don't modify any real stuff until the save occurs
            $scope.article = angular.copy(article);
        },
        function() {
            $scope.errors.push("Could not load the article.");
        });

    // setup editor options
    $scope.editorOptions = {
        "filebrowserImageUploadUrl": "http://localhost:55005/api/image/Upload?targetPath=test",
        "extraPlugins": "imagebrowser",
        "imageBrowser_listUrl": "http://localhost:55005/api/imagelist"
    };

    $scope.saveArticle = function() {
        $scope.errors = [];

        var okToSave = true;

        if ( !$scope.article.title || $scope.article.title.length === 0) {
            $scope.errors.push("Please enter a title.");
            return false;
        }

        if ( articleService.articleWithTitleExists($scope.categoryId, $scope.article.title, null)) {
            $scope.errors.push("The specified title is already in use in this category, please choose another.");
            return false;
        }

        articleService.editArticle($scope.categoryId, $scope.article,
            function() {
                $location.path("/list/" + $scope.categoryId);
            },
            function(err) {
                $scope.errors.push( "Could not update the page.");
            });
    };
}]);

