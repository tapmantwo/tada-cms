var tada = tada || {};
tada.articles = tada.articles || {};
tada.articles.testArticleList = [
        {
            articleCategoryId: null,
            articleCategoryName: "Root",
            categories: [
                {
                    articleCategoryId: "4c1f44256fd14267be108cb045ea3c7c",
                    articleCategoryName: "Toms Reviews",
                    categories: [],
                    articles: [
                        {
                            articleId: "c8b60d789508409cbf6ce7a2b2586300",
                            articleTitle: "Manic Miner C64"
                        },
                        {
                            articleId: "cb5f8eb21d1045879b76f5ac1001626c",
                            articleTitle: "Short Circuit C64"
                        },
                        {
                            articleId: "deda4f91e721471da9e6c09862b27f99",
                            articleTitle: "Crystal Castles Atari 2600"
                        },
                        {
                            articleId: "4bc9077e02d6455eb48e7c4c4e8a9086",
                            articleTitle: "Yars Revenge Atari 2600"
                        }
                    ]
                },
                {
                    articleCategoryId: "088a2fb507f54b9bae8608067f18934f",
                    articleCategoryName: "Richards Reviews",
                    categories: [],
                    articles: [
                        {
                            articleId: "03823d5f66574e6b86c34ba418775955",
                            articleTitle: "Manic Miner C64"
                        },
                        {
                            articleId: "c26c2a80f721428bbf53b7211a84807a",
                            articleTitle: "Short Circuit C64"
                        },
                        {
                            articleId: "4ba6ceb62cc4497394c965656849c0e6",
                            articleTitle: "Crystal Castles Atari 2600"
                        },
                        {
                            articleId: "8f1c8b91f85e4e9dbc15e1a0a0afeb95",
                            articleTitle: "Yars Revenge Atari 2600"
                        }
                    ]
                }
            ],
            articles: [
                {
                    articleId: "e0542205b29c4905980db3a765805716",
                    articleTitle: "Home page"
                }
            ]
        }
    ];