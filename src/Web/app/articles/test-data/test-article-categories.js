var tada = tada || {};
tada.articles = tada.articles || {};
tada.articles.testArticleCategories = [
        {
            parentCategoryId: null,
            parentCategoryName: null,
            articleCategoryId: "4c1f44256fd14267be108cb045ea3c7c",
            name: "Toms Reviews",
        },
        {
            parentCategoryId: null,
            parentCategoryName: null,
            articleCategoryId: "088a2fb507f54b9bae8608067f18934f",
            name: "Richards Reviews",
        }
    ];