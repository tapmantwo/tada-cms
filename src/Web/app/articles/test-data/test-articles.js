var tada = tada || {};
tada.articles = tada.articles || {};
tada.articles.testArticles = [
        {
            articleId: "c8b60d789508409cbf6ce7a2b2586300",
            title: "Manic Miner C64",
            summary: "Tom reviews Manic Miner on the C64",
            content: "<p>It is <strong>good</strong>.<p>"
        },
        {
            articleId: "cb5f8eb21d1045879b76f5ac1001626c",
            title: "Short Circuit C64",
            summary: "Tom reviews Short Circuit on the C64",
            content: "<p>It is <strong>okay</strong>.<p>"
        },
        {
            articleId: "deda4f91e721471da9e6c09862b27f99",
            title: "Crystal Castles Atari 2600",
            summary: "Tom reviews Crystal Castles on the Atari 2600",
            content: "<p>It is <strong>fun</strong>.<p>"
        },
        {
            articleId: "4bc9077e02d6455eb48e7c4c4e8a9086",
            title: "Yars Revenge Atari 2600",
            summary: "Tom reviews Yars Revenge on the Atari 2600",
            content: "<p>It is <strong>awesome</strong>.<p>"
        },
        {
            articleId: "03823d5f66574e6b86c34ba418775955",
            title: "Manic Miner C64",
            summary: "Richard reviews Manic Miner on the C64",
            content: "<p>blah, blah, blah<p>"
        },
        {
            articleId: "c26c2a80f721428bbf53b7211a84807a",
            title: "Short Circuit C64",
            summary: "Richard reviews Short Circuit on the C64",
            content: "<p>Foo foo foo<p>"
        },
        {
            articleId: "4ba6ceb62cc4497394c965656849c0e6",
            title: "Crystal Castles Atari 2600",
            summary: "Richard reviews Crystal Castles on the Atari 2600",
            content: "<p>Wakkah Wakkah Wakkah<p>"
        },
        {
            articleId: "8f1c8b91f85e4e9dbc15e1a0a0afeb95",
            title: "Yars Revenge Atari 2600",
            summary: "Richard reviews Yars Revenge on the Atari 2600",
            content: "<p>Flibble Flibbe <em>pop</em>.<p>"
        },
        {
            articleId: "e0542205b29c4905980db3a765805716",
            title: "Home page",
            summary: "The home page",
            content: "<p>Welcome to our site<p>"
        }
    ];