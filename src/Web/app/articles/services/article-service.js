var tada = tada || {};
tada.articles = tada.articles || {};
tada.articles.factory("articleService", ["$resource", "serviceUris", "runtime", "guidGenerator", "arrayUtils", function ($resource, serviceUris, runtime, guidGenerator, arrayUtils) {
    "use strict";

    function findCategory(categoryId, categories)
    {
        var category = arrayUtils.single(categories, function(cat) {
            return cat.articleCategoryId === categoryId;
        });

        if ( !category ) {
            // Check subcategories instead then
            for( var i = 0; i < categories.length; i++ ) {
                var subCategory = findCategory(categoryId, categories[i].categories);

                if ( subCategory ) {
                    category = subCategory;
                    break;
                }
            }
        }

        return category;
    }

    return {
        getArticleList: function(onSuccess, onFailure) {
            if (runtime.useLiveData()) {
                // todo
                onFailure(500);
            } else {
                onSuccess(tada.articles.testArticleList);
            }
        },

        addCategory: function(parentId, category, onSuccess, onFailure) {
            if (runtime.useLiveData()) {
                // todo
                onFailure(500);
            } else {
                var parent = findCategory(parentId, tada.articles.testArticleList);
                if ( parent ) {
                    category.articleCategoryId = guidGenerator.makeGuid();
                    parent.categories.push(category);
                    tada.articles.testArticleCategories.push( {
                        parentCategoryId: parentId,
                        parentCategoryName: parent.name,
                        articleCategoryId: category.articleCategoryId,
                        name: category.name
                    });

                    onSuccess(category.articleCategoryId);
                    return;
                }

                onFailure("foo");
                return;
            }
        },

        editCategory: function(category, onSuccess, onFailure) {
            if (runtime.useLiveData()) {
                // todo
                onFailure(500);
            } else {
                var original = findCategory(category.articleCategoryId, tada.articles.testArticleList);
                if ( original ) {
                    original = category;

                    var testCategory = arrayUtils.single(tada.articles.testArticleCategories, function(c) {
                        return c.articleCategoryId === category.articleCategoryId;
                    });

                    if ( testCategory ) {
                        testCategory.name = category.name;
                    }

                    onSuccess();
                    return;
                }

                onFailure("foo");
                return;
            }
        },

        getCategory: function(categoryId, onSuccess, onFailure)
        {
            if (runtime.useLiveData()) {
                // todo
                onFailure(500);
            } else {
                var category = findCategory(categoryId, tada.articles.testArticleList);
                if ( category ) {
                    onSuccess(category);
                } else {
                    onFailure("404");
                }
            }
        },


        categoryWithNameExists: function(parentId, categoryName, exceptCategoryId)
        {
            if (runtime.useLiveData()) {
                // todo
                return true;
            } else {
                var parent = findCategory(parentId, tada.articles.testArticleList);
                if ( parent ) {
                    var match = arrayUtils.single(parent.categories, function(cat) {
                        return cat.articleCategoryName === categoryName && cat.articleCategoryId !== exceptCategoryId;
                    });

                    return match !== null;
                }

                return false;
            }
        },

        addArticle: function(categoryId, article, onSuccess, onFailure)
        {
            if (runtime.useLiveData()) {
                // todo
                return onFailure(500);
            } else {
                var category = findCategory(categoryId, tada.articles.testArticleList);
                if ( category ) {
                    article.articleId = guidGenerator.makeGuid();
                    tada.articles.testArticles.push(article);
                    category.articles.push( {
                        articleId: article.articleId,
                        articleTitle: article.title
                    });
                    onSuccess(article.articleId);
                } else {
                    onFailure(404);
                }
            }
        },

        editArticle: function(categoryId, article, onSuccess, onFailure)
        {
            if (runtime.useLiveData()) {
                // todo
                return onFailure(500);
            } else {
                var category = findCategory(categoryId, tada.articles.testArticleList);
                if ( category ) {
                    arrayUtils.replace(tada.articles.testArticles, function(a) {
                        return a.articleId === article.articleId;
                    }, article);

                    arrayUtils.replace(category.articles, function(al) {
                        return al.articleId === article.articleId;
                    },  {
                        articleId: article.articleId,
                        articleTitle: article.title
                    });

                    onSuccess(article.articleId);
                } else {
                    onFailure(404);
                }
            }
        },

        deleteArticle:  function(categoryId, articleId, onSuccess, onFailure)
        {
            if (runtime.useLiveData()) {
                // todo
                return onFailure(500);
            } else {
                var category = findCategory(categoryId, tada.articles.testArticleList);
                if ( category ) {
                    arrayUtils.singleAndSplice(tada.articles.testArticles, function(a) {
                        return a.articleId === articleId;
                    });

                    arrayUtils.singleAndSplice(category.articles, function(al) {
                        return al.articleId === articleId;
                    });

                    onSuccess(articleId);
                } else {
                    onFailure(404);
                }
            }
        },

        articleWithTitleExists: function(categoryId, title, exceptArticleId)
        {
            if (runtime.useLiveData()) {
                // todo
                return true;
            } else {
                var category = findCategory(categoryId, tada.articles.testArticleList);
                if ( category ) {
                    var match = arrayUtils.single(category.articles, function(article) {
                        return article.title === title && article.articleId !== exceptArticleId;
                    });

                    return match !== null;
                }

                return false;
            }
        },

        getArticle: function(articleId, onSuccess, onFailure) {
            if (runtime.useLiveData()) {
                // todo
                return onFailure(500);
            } else {
                var article = arrayUtils.single(tada.articles.testArticles, function(article) {
                    return article.articleId === articleId;
                });
                if ( article ) {
                    onSuccess(article);
                } else {
                    onFailure(404);
                }
            }
        }

    };
}]);