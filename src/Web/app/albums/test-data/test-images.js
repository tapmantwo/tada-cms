var tada = tada || {};
tada.albums = tada.albums || {};
tada.albums.testImages = [
    {
        image: "https://www.google.co.uk/images/srpr/logo11w.png",
        thumb: "https://www.google.co.uk/images/srpr/logo11w.png",
        folder: "\\root"
    },
    {
        image: "http://static.carbonads.com/Advertisers/74394d44308649468fa312afdeaa27cd.png",
        thumb: "http://static.carbonads.com/Advertisers/74394d44308649468fa312afdeaa27cd.png",
        folder: "\\root\\pop"
    },
    {
        image: "http://localhost:55005/test/1.jpg",
        thumb: "http://localhost:55005/test/1_thumb.jpg",
        folder: "\\root"
    },
    {
        image: "http://localhost:55005/test/2.jpg",
        thumb: "http://localhost:55005/test/2_thumb.jpg",
        folder: "\\root"
    },
    {
        image: "http://localhost:55005/test/3.jpg",
        thumb: "http://localhost:55005/test/3_thumb.jpg",
        folder: "\\root"
    },
    {
        image: "http://localhost:55005/test/4.jpg",
        thumb: "http://localhost:55005/test/4_thumb.jpg",
        folder: "\\root"
    }
];