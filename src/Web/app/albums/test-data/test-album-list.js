var tada = tada || {};
tada.albums = tada.albums || {};
tada.albums.testAlbumList = [
    {
        albumCategoryId: null,
        albumCategoryName: "Root",
        categories: [
            {
                albumCategoryId: "3e48583e0c50480796e6b01d728ae4c5",
                albumCategoryName: "General",
                categories: [],
                albums: [
                    {
                        albumId: "901205c8b0b940baac6e833b2cab0468",
                        albumTitle: "Home",
                        deleted: false
                    }
                ]
            }
        ],
        albums: [
            {
                albumId: "4c323dc18e404ec6aa642eabb60568eb",
                albumTitle: "Reviews",
                deleted: false
            }
        ]
    }
];