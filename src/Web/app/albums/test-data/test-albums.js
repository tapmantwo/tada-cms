var tada = tada || {};
tada.albums = tada.albums || {};
tada.albums.testAlbums = [
    {
        albumId: "901205c8b0b940baac6e833b2cab0468",
        title: "Home",
        width: "500",
        height: "500",
        images: [
            {
                title: "Title",
                description: "Description",
                image: "https://www.google.co.uk/images/srpr/logo11w.png",
                thumb: "https://www.google.co.uk/images/srpr/logo11w.png",
                folder: "\\root"
            },
            {
                title: "Title",
                description: "Description",
                image: "http://localhost:55005/test/1.jpg",
                thumb: "http://localhost:55005/test/1_thumb.jpg",
                folder: "\\root"
            },
            {
                title: "Title",
                description: "Description",
                image: "http://localhost:55005/test/2.jpg",
                thumb: "http://localhost:55005/test/2_thumb.jpg",
                folder: "\\root"
            },
            {
                title: "Title",
                description: "Description",
                image: "http://localhost:55005/test/3.jpg",
                thumb: "http://localhost:55005/test/3_thumb.jpg",
                folder: "\\root"
            },
            {
                title: "Title",
                description: "Description",
                image: "http://localhost:55005/test/4.jpg",
                thumb: "http://localhost:55005/test/4_thumb.jpg",
                folder: "\\root"
            }
        ],
        deleted: false
    },
    {
        albumId: "4c323dc18e404ec6aa642eabb60568eb",
        title: "Reviews",
        images: [
            {
                title: "Title",
                description: "Description",
                image: "https://www.google.co.uk/images/srpr/logo11w.png",
                thumb: "https://www.google.co.uk/images/srpr/logo11w.png",
                folder: "\\root"
            }
        ],
        deleted: false
    }
];