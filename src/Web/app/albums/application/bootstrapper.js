var tada = tada || {};
tada.albums = angular.module("albumsModule", ["global", "ngResource", "ngRoute"])
    .config(["$routeProvider", function($routeProvider) {
        "use strict";
        $routeProvider.when("/list/:categoryId?", {
            templateUrl: "./templates/list.html",
            controller: "albumListController"
        });

        $routeProvider.when("/addCategory/:parentCategoryId", {
            templateUrl: "./templates/new-category.html",
            controller: "newCategoryController",
        });

        $routeProvider.when("/editCategory/:parentCategoryId/:categoryId", {
            templateUrl: "./templates/edit-category.html",
            controller: "editCategoryController",
        });

        $routeProvider.when("/editAlbum/:categoryId/:albumId", {
            templateUrl: "./templates/edit-album.html",
            controller: "editAlbumController",
        });

        $routeProvider.when("/addAlbum/:categoryId", {
            templateUrl: "./templates/new-album.html",
            controller: "newAlbumController",
        });

        $routeProvider.when("/deleteAlbum/:categoryId/:albumId", {
            templateUrl: "./templates/delete-album.html",
            controller: "deleteAlbumController",
        });

        $routeProvider.otherwise({ redirectTo: "/list" });
    }]);
