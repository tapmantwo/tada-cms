var tada = tada || {};
tada.albums = tada.albums || {};
tada.albums.factory("albumService", ["$resource", "serviceUris", "runtime", "guidGenerator", "arrayUtils", function ($resource, serviceUris, runtime, guidGenerator, arrayUtils) {
    "use strict";

    function findCategory(categoryId, categories)
    {
        var category = arrayUtils.single(categories, function(cat) {
            return cat.albumCategoryId === categoryId;
        });

        if ( !category ) {
            // Check subcategories instead then
            for( var i = 0; i < categories.length; i++ ) {
                var subCategory = findCategory(categoryId, categories[i].categories);

                if ( subCategory ) {
                    category = subCategory;
                    break;
                }
            }
        }

        return category;
    }

    function findAlbum(albumId, albums)
    {
        var album = arrayUtils.single(albums, function(album) {
            return album.albumId === albumId;
        });

        return album;
    }

    function findAlbumInAlbumList(albumId, albumList)
    {
        var album = arrayUtils.single(albumList.albums, function(p) {
            return p.albumId === albumId;
        });

        if ( !album ) {
            // Check subcategories instead then
            for( var i = 0; i < albumList.categories.length; i++ ) {
                album = findAlbumInAlbumList(albumId, albumList.categories[i]);

                if ( album ) {
                    break;
                }
            }
        }

        return album;
    }

    return {
        getAlbumList: function(onSuccess, onFailure) {
            if (runtime.useLiveData()) {
                // todo
                onFailure(500);
            } else {
                onSuccess(tada.albums.testAlbumList);
            }
        },

        getAlbumCategories: function(onSuccess, onFailure)
        {
            if (runtime.useLiveData()) {
                // todo
                onFailure(500);
            } else {
                onSuccess(tada.albums.testAlbumCategories);
            }
        },

        addCategory: function(parentId, category, onSuccess, onFailure) {
            if (runtime.useLiveData()) {
                // todo
                onFailure(500);
            } else {
                var parent = findCategory(parentId, tada.albums.testAlbumList);
                if ( parent ) {
                    category.albumCategoryId = guidGenerator.makeGuid();
                    parent.categories.push(category);
                    tada.albums.testAlbumCategories.push( {
                        parentCategoryId: parentId,
                        parentCategoryName: parent.name,
                        albumCategoryId: category.albumCategoryId,
                        name: category.name
                    });

                    onSuccess(category.albumCategoryId);
                    return;
                }

                onFailure("foo");
                return;
            }
        },

        editCategory: function(category, onSuccess, onFailure) {
            if (runtime.useLiveData()) {
                // todo
                onFailure(500);
            } else {
                var original = findCategory(category.albumCategoryId, tada.albums.testAlbumList);
                if ( original ) {
                    original = category;

                    var testCategory = arrayUtils.single(tada.albums.testAlbumCategories, function(c) {
                        return c.albumCategoryId === category.albumCategoryId;
                    });

                    if ( testCategory ) {
                        testCategory.name = category.name;
                    }

                    onSuccess();
                    return;
                }

                onFailure("foo");
                return;
            }
        },

        categoryWithNameExists: function(parentId, categoryName, exceptCategoryId)
        {
            if (runtime.useLiveData()) {
                // todo
                return true;
            } else {
                var parent = findCategory(parentId, tada.albums.testAlbumList);
                if ( parent ) {
                    var match = arrayUtils.single(parent.categories, function(cat) {
                        return cat.albumCategoryName === categoryName && cat.albumCategoryId !== exceptCategoryId;
                    });

                    return match !== null;
                }

                return false;
            }
        },

        albumWithTitleExists: function(categoryId, albumTitle, exceptAlbumId)
        {
            if (runtime.useLiveData()) {
                // todo
                return true;
            } else {
                var category = findCategory(categoryId, tada.albums.testAlbumList);
                if ( category ) {
                    var match = arrayUtils.single(category.albums, function(album) {
                        return album.title === albumTitle && album.albumId !== exceptAlbumId;
                    });

                    return match !== null;
                }

                return false;
            }
        },

        getCategory: function(categoryId, onSuccess, onFailure)
        {
            if (runtime.useLiveData()) {
                // todo
                onFailure(500);
            } else {
                var category = findCategory(categoryId, tada.albums.testAlbumList);
                if ( category ) {
                    onSuccess(category);
                } else {
                    onFailure("404");
                }
            }
        },

        getAlbum: function(albumId, onSuccess, onFailure)
        {
            if (runtime.useLiveData()) {
                // todo
                onFailure(500);
            } else {
                var album = findAlbum(albumId, tada.albums.testAlbums);
                if ( album ) {
                    onSuccess(album);
                } else {
                    onFailure("404");
                }
            }
        },

        getImages: function (onSuccess, onFailure) {
            if (runtime.useLiveData()) {
                // todo
                onFailure(500);
            } else {
                onSuccess(tada.albums.testImages);
            }
        },
        
        addAlbum: function(categoryId, album, onSuccess, onFailure)
        {
            if (runtime.useLiveData()) {
                // todo
                onFailure(500);
            } else {
                album.albumId = guidGenerator.makeGuid();
                tada.albums.testAlbums.push(album);

                var category = findCategory(categoryId, tada.albums.testAlbumList);
                if ( category !== null ) {
                    category.albums.push(  {
                        albumId: album.albumId,
                        albumTitle: album.title,
                    });
                    onSuccess(album.albumId);
                    return;
                }

                onFailure(500);
            }
        },

        editAlbum: function(album, onSuccess, onFailure)
        {
            if (runtime.useLiveData()) {
                // todo
                onFailure(500);
            } else {
                var existingAlbum = findAlbum(album.albumId, tada.albums.testAlbums);
                if (existingAlbum) {
                    existingAlbum = album;

                    var albumListEntry = findAlbumInAlbumList(album.albumId, tada.albums.testAlbumList[0]);
                    if ( albumListEntry ) {
                        albumListEntry.albumTitle = album.title;
                        onSuccess(album.albumId);
                    } else {
                        onFailure(404);
                    }
                } else {
                    onFailure(404);
                }
            }
        },

        deleteAlbum: function(albumId, onSuccess, onFailure)
        {
            if (runtime.useLiveData()) {
                // todo
                onFailure(500);
            } else {
                var existingAlbum = findAlbum(albumId, tada.albums.testAlbums);
                if (existingAlbum) {
                    existingAlbum.deleted = true;

                    var albumListEntry = findAlbumInAlbumList(albumId, tada.albums.testAlbumList[0]);
                    if ( albumListEntry ) {
                        albumListEntry.deleted = true;
                        onSuccess();
                    } else {
                        onFailure(404);
                    }
                } else {
                    onFailure(404);
                }
            }
        }
    };
}]);