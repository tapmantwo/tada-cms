var tada = tada || {};
tada.albums = tada.albums || {};
tada.albums.controller("newCategoryController", ["$scope", "albumService", "$routeParams", "$location", function($scope, albumService, $routeParams, $location) {
    "use strict";

    $scope.errors = [];
    $scope.categoryName = "";
    $scope.parentCategoryId = $routeParams.parentCategoryId || null;

    $scope.addCategory = function() {
        // Validate the category name is unique for this parent
        $scope.errors = [];

        if ( albumService.categoryWithNameExists($scope.parentCategoryId, $scope.categoryName, null)) {
            $scope.errors.push("The specified name is already in use, please choose another.");
            return false;
        }

        // create
        var category =  {
            albumCategoryId: null,
            albumCategoryName: $scope.categoryName,
            categories: [],
            albums: []
        };

        albumService.addCategory($scope.parentCategoryId, category,
            function(id) {
                $location.path("/list/" + id);
            },
            function(err) {
                $scope.errors.push("Could not add the category.");
            });
    };
}]);

