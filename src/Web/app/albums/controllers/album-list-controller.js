var tada = tada || {};
tada.albums = tada.albums || {};
tada.albums.controller("albumListController", ["$scope", "albumService", "$routeParams", "arrayUtils", "$location", function($scope, albumService, $routeParams, arrayUtils, $location) {
    "use strict";

    $scope.currentCategory = null;
    $scope.categoryId = $routeParams.categoryId || null;
    $scope.showDeleted = false;
    $scope.deletedAlbumCount = 0;
    $scope.albumList = null;
    $scope.previousCategories = [];
    $scope.previousCategory = null;

    albumService.getAlbumList(function(result) {
        $scope.albumList = result;

        if ( $scope.categoryId ) {
            // Find the specified category
            var route = [];
            var category = findCategory($scope.categoryId, result, route);
            if ( category ) {
                $scope.previousCategory = route[0];
                $scope.previousCategories = arrayUtils.reverse(route);
                $scope.currentCategory = category;
            } else {
                $scope.previousCategories = [];
                $scope.currentCategory = result[0];
            }
        } else {
            $scope.previousCategories = [];
            $scope.currentCategory = result[0];
        }
        $scope.categoryId = $scope.currentCategory.albumCategoryId;

        var deletedCount = 0;
        for( var i = 0; i < $scope.currentCategory.albums.length; i++ ) {
            if ( $scope.currentCategory.albums[i].deleted ) {
                deletedCount++;
            }
        }

        $scope.deletedAlbumCount = deletedCount;
    });
    
    $scope.shouldShowAlbum = function(album)
    {
        return !album.deleted || $scope.showDeleted;
    };

    $scope.selectCategory = function(category)
    {
        if ( category.albumCategoryId ) {
            $location.path("/list/" + category.albumCategoryId);
        } else {
            $location.path("/list");
        }
    };

    $scope.selectAlbum = function(album)
    {
        if($scope.categoryId) {
            $location.path("/editAlbum/" + $scope.categoryId + "/" + album.albumId);
        } else {
            $location.path("/editAlbum//" + album.albumId);
        }
    };

    function findCategory(categoryId, categories, route)
    {
        var category = null;

        var i;
        for( i = 0; i < categories.length; i++ ) {
            if ( categories[i].albumCategoryId === categoryId ) {
                category = categories[i];
                break;
            }
        }

        if ( !category ) {
            // Check subcategories instead then
            for( i = 0; i < categories.length; i++ ) {
                var subCategory = findCategory(categoryId, categories[i].categories, route);
                if ( subCategory ) {
                    category = subCategory;
                    route.push(categories[i]);
                    break;
                }
            }
        }

        return category;
    }
}]);

