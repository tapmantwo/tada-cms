var tada = tada || {};
tada.albums = tada.albums || {};
tada.albums.controller("deleteAlbumController", ["$scope", "albumService", "$routeParams", "$location", function($scope, albumService, $routeParams, $location) {
    "use strict";

    $scope.errors = [];
    $scope.albumId = $routeParams.albumId || null;
    $scope.categoryId = $routeParams.categoryId || null;
    $scope.album = null;

    albumService.getAlbum($scope.albumId,
        function(result) {
            $scope.album = result;
        },
        function(err) {
            $scope.errors.push("Could not load album.");
        });

    $scope.deleteAlbum = function() {
        // Validate the category name is unique for this parent
        $scope.errors = [];

        albumService.deleteAlbum($scope.albumId,
            function(id) {
                $location.path("/list/" + $scope.categoryId);
            },
            function(err) {
                $scope.errors.push("Could not delete the album.");
            });
    };
}]);

