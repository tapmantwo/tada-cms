var tada = tada || {};
tada.albums = tada.albums || {};
tada.albums.controller("editCategoryController", ["$scope", "albumService", "$routeParams", "$location", function($scope, albumService, $routeParams, $location) {
    "use strict";

    $scope.errors = [];
    $scope.category = null;
    $scope.editCategoryId = $routeParams.categoryId;
    $scope.parentCategoryId = $routeParams.parentCategoryId || null;

    albumService.getCategory($scope.editCategoryId,
        function(result) {
            $scope.category = result;
        },
        function(err) {
            $scope.errors.push( "Could not load the category.");
        });

    $scope.editCategory = function() {
        // Validate the category name is unique for this parent
        $scope.errors = [];

        if (albumService.categoryWithNameExists($scope.parentCategoryId, $scope.category.albumCategoryName, $scope.editCategoryId)) {
            $scope.errors.push("The specified name is already in use, please choose another.");
            return false;
        }

        albumService.editCategory($scope.category,
            function() {
                $location.path("/list/" + $scope.category.albumCategoryId);
            },
            function(err) {
                $scope.errors.push( "Could not update the category.");
            });
    };
}]);

