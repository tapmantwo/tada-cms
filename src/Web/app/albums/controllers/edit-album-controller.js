var tada = tada || {};
tada.albums = tada.albums || {};
tada.albums.controller("editAlbumController", ["$scope", "albumService", "$routeParams", "$location", "arrayUtils", function ($scope, albumService, $routeParams, $location, arrayUtils) {
    "use strict";

    $scope.errors = [];
    $scope.showErrors = false;
    $scope.categoryId = $routeParams.categoryId || null;
    $scope.editAlbumId = $routeParams.albumId;
    $scope.album = null;
    $scope.albumCategories = [];
    $scope.images = [];
    $scope.imageModalShown = false;
    $scope.imageSelectErrors = [];

    $scope.$watchCollection("errors", function (newCol, oldCol, scope) {
        $scope.showErrors = newCol.length > 0;
    });
    
    albumService.getAlbum($scope.editAlbumId,
        function (result) {
            $scope.album = result;
        },
        function (err) {
            $scope.errors.push("Could not load the image gallery.");
        });

    albumService.getAlbumCategories(
        function (result) {
            $scope.albumCategories = result;
        },
        function (err) {
            $scope.errors.push("Could not load the image gallery categories.");
        });

    albumService.getImages(
        function (result) {
            $scope.images = result;
        },
        function (err) {
            $scope.errors.push("Could not load the images.");
        });

    $scope.showAvailableImages = function () {
        $scope.imageModalShown = true;
    };

    $scope.removeImage = function (idx) {
        $scope.album.images.splice(idx, 1);
        return;
    };

    $scope.addImage = function (image) {
        $scope.imageSelectErrors = [];

        var alreadyExists = false;
        for (var imageIndex = $scope.album.images.length - 1; imageIndex > -1; imageIndex--) {
            if ($scope.album.images[imageIndex].image === image.image) {
                alreadyExists = true;
            }
        }

        if (alreadyExists) {
            $scope.imageSelectErrors.push("Image already in album");
            return;
        } else {
            var title = prompt("Set title");
            if (!title || title.length === 0) {
                $scope.imageSelectErrors.push("A title is required");
                return;
            }
            var description = prompt("Set description");

            image.title = title;
            image.description = description;
            $scope.album.images.push(image);
            $scope.imageModalShown = false;
            return;
        }
    };

    $scope.editImage = function (idx) {
        $scope.album.images[idx].title = prompt("Edit title", $scope.album.images[idx].title);
        $scope.album.images[idx].description = prompt("Edit description", $scope.album.images[idx].description);
        return;
    };

    $scope.editAlbum = function () {
        $scope.errors = [];

        var okToSave = true;

        if (!$scope.album.width || $scope.album.width <= 0) {
            $scope.errors.push("A width must be entered.");
            okToSave = false;
        }

        if (!$scope.album.height || $scope.album.height <= 0) {
            $scope.errors.push("A height must be entered.");
            okToSave = false;
        }

        if (albumService.albumWithTitleExists($scope.categoryId, $scope.album.title, $scope.album.albumId)) {
            $scope.errors.push("The specified title is already in use in this category, please choose another.");
            okToSave = false;
        }

        if (!okToSave) {
            return false;
        }

        albumService.editAlbum($scope.album,
            function () {
                $location.path("/list/" + $scope.categoryId);
            },
            function (err) {
                $scope.errors.push("Could not update the album.");
            });
    };
}]);

