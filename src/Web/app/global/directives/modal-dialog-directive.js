﻿angular.module("ng").directive("modalDialog", function () {
    "use strict";
    return {
        restrict: "E",
        scope: {
            title: "@",
            errors: "=",
            okClicked: "&okClicked",
            cancelClicked: "&cancelClicked",
            show: "=",
            hideOk: "=",
            hideCancel: "="
        },
        replace: true, // Replace with the template below
        transclude: true, // we want to insert custom content inside the directive
        link: function ($scope, element, attrs) {

            $scope.showErrors = false;

            $scope.showTitle = $scope.title.length > 0;

            $scope.$watchCollection("errors", function (newCol, oldCol, scope) {
                $scope.showErrors = newCol.length > 0;
            });

            $scope.$watch("show", function (value) {
                if (value) {
                    $(element).modal("show");
                } else {
                    $(element).modal("hide");
                }
            });

            $scope.hideModal = function () {
                $scope.cancelClicked();
                $scope.show = false;
                $scope.errors = [];
            };

            $scope.okClick = function () {
                if ($scope.okClicked()) {
                    $scope.show = false;
                    $scope.errors = [];
                    return;
                }
            };
        },
        templateUrl: "../global/templates/directives/modal-dialog-directive.html"
    };
});