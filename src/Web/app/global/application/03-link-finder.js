var tada = tada || {};
tada.linkFinder = function() {
	"use strict";
	return {
		searcher: new tada.arrayUtils(),
		findLink: function (entity, rel) {
			if ( entity !== null && typeof entity !== "undefined" ) {
				var links = entity.links;
				if ( links !== null && links instanceof Array ) {
					return this.searcher.single(links, function(link) {
						return link.rel === rel;
					});
				}
			}
			return null;
		}
	};
};