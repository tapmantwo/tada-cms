var tada = tada || {};
tada.arrayUtils = function() {
	"use strict";
	return {
		single: function (array, compareFunc) {
			if (!array) {
                return null;
            }
            for ( var index = 0; index < array.length; index++) {
				var item = array[index];
				if ( compareFunc(item) ) {
					return item;
				}
			}
			return null;
		},

		all: function (array, compareFunc) {
            if (!array) {
                return null;
            }
			var matches = [];
			for ( var index = 0; index < array.length; index++) {
				var item = array[index];
				if ( compareFunc(item) ) {
					matches.push(item);
				}
			}
			return matches;
		},

        singleAndSplice: function (array, compareFunc) {
            if (!array) {
                return null;
            }
            for ( var index = 0; index < array.length; index++) {
                var item = array[index];
                if ( compareFunc(item) ) {
                    array.splice(index, 1);
                }
            }
            return null;
        },

        indexOf: function(array, compareFunc) {
            if (!array) {
                return -1;
            }
            for ( var index = 0; index < array.length; index++) {
                var item = array[index];
                if ( compareFunc(item) ) {
                    return index;
                }
            }
            return -1;
        },

        replace: function(array, compareFunc, newValue)
        {
            this.singleAndSplice(array, compareFunc);
            array.push(newValue);
        },

        reverse: function(array) {
            var reversed = [];
            for( var i = array.length - 1; i >= 0; i-- ) {
                reversed.push(array[i]);
            }
            return reversed;
        }
	};
};