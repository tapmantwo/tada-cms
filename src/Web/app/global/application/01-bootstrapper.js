var tada = tada || {};
tada.global = angular.module("global", ["ngResource"])
    .constant("keyboardConstants", {
        BACKSPACE: 8,
        TAB: 9,
        ENTER: 13,
        SHIFT: 16,
        CTRL: 17,
        ALT: 18,
        PAUSE: 19,
        CAPS_LOCK: 20,
        ESCAPE: 27,
        SPACE: 32,
        PAGE_UP: 33,
        PAGE_DOWN: 34,
        END: 35,
        HOME: 36,
        LEFT_ARROW: 37,
        UP_ARROW: 38,
        RIGHT_ARROW: 39,
        DOWN_ARROW: 40,
        INSERT: 45,
        DELETE: 46
    })
    .constant("modernizr", window.Modernizr)
    .factory("linkFinder", [function () {
        "use strict";
        return new tada.linkFinder();
    }])
    .factory("arrayUtils", [function () {
        "use strict";
        return new tada.arrayUtils();
    }])
    .factory("guidGenerator", [function() {
        "use strict";
        return  {
            makeGuid: function() {
                var d = new Date().getTime();
                var uuid = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(c) {
                    var r = (d + Math.random()*16)%16 | 0;
                    d = Math.floor(d/16);
                    return (c==="x" ? r : (r&0x7|0x8)).toString(16);
                });
                return uuid;
            }
        };
    }])
    .factory("runtime",  ["$location", function($location) {
        "use strict";
        function isFileBased() {
            return $location.protocol() === "file";
        }

        return {
            useLiveData: function() {
                return !isFileBased();
            }
        };
    }])
    .value("serviceUris", {
        pagesUri: window.QUERY_HOST + "/pages",
    })
    .run([function() {
        "use strict";

        // some utility functions
        String.prototype.repeat = function (times) {
            return (new Array(times + 1)).join(this);
        };

        String.prototype.countOccurrances = function (value, upto) {
            var result = 0;
            var lastIndex = this.indexOf(value);
            while (lastIndex !== -1) {
                result++;
                lastIndex = this.indexOf(value, lastIndex + 1);
                if (upto && upto !== -1 && lastIndex > upto) {
                    break;
                }
            }
            return result;
        };

        String.prototype.indexToEndOf = function (value, position) {
            var index = this.indexOf(value, position);
            if (index > -1) {
                index += value.length;
            }
            return index;
        };
    }]);
