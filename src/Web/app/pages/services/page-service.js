var tada = tada || {};
tada.pages = tada.pages || {};
tada.pages.factory("pageService", ["$resource", "serviceUris", "runtime", "guidGenerator", "arrayUtils", function ($resource, serviceUris, runtime, guidGenerator, arrayUtils) {
    "use strict";

    function findCategory(categoryId, categories)
    {
        var category = arrayUtils.single(categories, function(cat) {
            return cat.pageCategoryId === categoryId;
        });

        if ( !category ) {
            // Check subcategories instead then
            for( var i = 0; i < categories.length; i++ ) {
                var subCategory = findCategory(categoryId, categories[i].categories);

                if ( subCategory ) {
                    category = subCategory;
                    break;
                }
            }
        }

        return category;
    }

    function findPage(pageId, pages)
    {
        var page = arrayUtils.single(pages, function(page) {
            return page.pageId === pageId;
        });

        return page;
    }

    function findPageInPageList(pageId, pageList)
    {
        var page = arrayUtils.single(pageList.pages, function(p) {
            return p.pageId === pageId;
        });

        if ( !page ) {
            // Check subcategories instead then
            for( var i = 0; i < pageList.categories.length; i++ ) {
                page = findPageInPageList(pageId, pageList.categories[i]);

                if ( page ) {
                    break;
                }
            }
        }

        return page;
    }

    return {
        getPageList: function(onSuccess, onFailure) {
            if (runtime.useLiveData()) {
                // todo
                onFailure(500);
            } else {
                onSuccess(tada.pages.testPageList);
            }
        },

        getPageCategories: function(onSuccess, onFailure)
        {
            if (runtime.useLiveData()) {
                // todo
                onFailure(500);
            } else {
                onSuccess(tada.pages.testPageCategories);
            }
        },

        addCategory: function(parentId, category, onSuccess, onFailure) {
            if (runtime.useLiveData()) {
                // todo
                onFailure(500);
            } else {
                var parent = findCategory(parentId, tada.pages.testPageList);
                if ( parent ) {
                    category.pageCategoryId = guidGenerator.makeGuid();
                    parent.categories.push(category);
                    tada.pages.testPageCategories.push( {
                        parentCategoryId: parentId,
                        parentCategoryName: parent.name,
                        pageCategoryId: category.pageCategoryId,
                        name: category.name
                    });

                    onSuccess(category.pageCategoryId);
                    return;
                }

                onFailure("foo");
                return;
            }
        },

        editCategory: function(category, onSuccess, onFailure) {
            if (runtime.useLiveData()) {
                // todo
                onFailure(500);
            } else {
                var original = findCategory(category.pageCategoryId, tada.pages.testPageList);
                if ( original ) {
                    original = category;

                    var testCategory = arrayUtils.single(tada.pages.testPageCategories, function(c) {
                        return c.pageCategoryId === category.pageCategoryId;
                    });

                    if ( testCategory ) {
                        testCategory.name = category.name;
                    }

                    onSuccess();
                    return;
                }

                onFailure("foo");
                return;
            }
        },

        categoryWithNameExists: function(parentId, categoryName, exceptCategoryId)
        {
            if (runtime.useLiveData()) {
                // todo
                return true;
            } else {
                var parent = findCategory(parentId, tada.pages.testPageList);
                if ( parent ) {
                    var match = arrayUtils.single(parent.categories, function(cat) {
                        return cat.pageCategoryName === categoryName && cat.pageCategoryId !== exceptCategoryId;
                    });

                    return match !== null;
                }

                return false;
            }
        },

        pageWithTitleExists: function(categoryId, pageTitle, exceptPageId)
        {
            if (runtime.useLiveData()) {
                // todo
                return true;
            } else {
                var category = findCategory(categoryId, tada.pages.testPageList);
                if ( category ) {
                    var match = arrayUtils.single(category.pages, function(page) {
                        return page.title === pageTitle && page.pageId !== exceptPageId;
                    });

                    return match !== null;
                }

                return false;
            }
        },

        pageWithUrlExists: function(url, exceptPageId)
        {
            if (runtime.useLiveData()) {
                // todo
                return true;
            } else {
                var match = arrayUtils.single(tada.pages.testPages, function(page) {
                    return page.url === url && page.pageId !== exceptPageId;
                });

                return match !== null;
            }
        },

        getCategory: function(categoryId, onSuccess, onFailure)
        {
            if (runtime.useLiveData()) {
                // todo
                onFailure(500);
            } else {
                var category = findCategory(categoryId, tada.pages.testPageList);
                if ( category ) {
                    onSuccess(category);
                } else {
                    onFailure("404");
                }
            }
        },

        getPage: function(pageId, onSuccess, onFailure)
        {
            if (runtime.useLiveData()) {
                // todo
                onFailure(500);
            } else {
                var page = findPage(pageId, tada.pages.testPages);
                if ( page ) {
                    onSuccess(page);
                } else {
                    onFailure("404");
                }
            }
        },

        getPageTemplates: function(onSuccess, onFailure)
        {
            if (runtime.useLiveData()) {
                // todo
                onFailure(500);
            } else {
                onSuccess(tada.pages.testPageTemplates);
            }
        },

        getArticles: function(onSuccess, onFailure)
        {
            if (runtime.useLiveData()) {
                // todo
                onFailure(500);
            } else {
                onSuccess(tada.pages.testArticles);
            }
        },

        getImageLists: function(onSuccess, onFailure)
        {
            if (runtime.useLiveData()) {
                // todo
                onFailure(500);
            } else {
                onSuccess(tada.pages.testImageLists);
            }
        },

        addPage: function(categoryId, page, onSuccess, onFailure)
        {
            if (runtime.useLiveData()) {
                // todo
                onFailure(500);
            } else {
                page.pageId = guidGenerator.makeGuid();
                tada.pages.testPages.push(page);

                var category = findCategory(categoryId, tada.pages.testPageList);
                if ( category !== null ) {
                    category.pages.push(  {
                        pageId: page.pageId,
                        pageTitle: page.title,
                    });
                    onSuccess(page.pageId);
                    return;
                }

                onFailure(500);
            }
        },

        editPage: function(page, onSuccess, onFailure)
        {
            if (runtime.useLiveData()) {
                // todo
                onFailure(500);
            } else {
                var existingPage = findPage(page.pageId, tada.pages.testPages);
                if (existingPage) {
                    existingPage = page;

                    var pageListEntry = findPageInPageList(page.pageId, tada.pages.testPageList[0]);
                    if ( pageListEntry ) {
                        pageListEntry.pageTitle = page.title;
                        onSuccess(page.pageId);
                    } else {
                        onFailure(404);
                    }
                } else {
                    onFailure(404);
                }
            }
        },

        deletePage: function(pageId, onSuccess, onFailure)
        {
            if (runtime.useLiveData()) {
                // todo
                onFailure(500);
            } else {
                var existingPage = findPage(pageId, tada.pages.testPages);
                if (existingPage) {
                    existingPage.deleted = true;

                    var pageListEntry = findPageInPageList(pageId, tada.pages.testPageList[0]);
                    if ( pageListEntry ) {
                        pageListEntry.deleted = true;
                        onSuccess();
                    } else {
                        onFailure(404);
                    }
                } else {
                    onFailure(404);
                }
            }
        }
    };
}]);