var tada = tada || {};
tada.pages = angular.module("pagesModule", ["global", "ngResource", "ngRoute"])
    .config(["$routeProvider", function($routeProvider) {
        "use strict";
        $routeProvider.when("/list/:categoryId?", {
            templateUrl: "./templates/list.html",
            controller: "pageListController"
        });

        $routeProvider.when("/addCategory/:parentCategoryId", {
            templateUrl: "./templates/new-category.html",
            controller: "newCategoryController",
        });

        $routeProvider.when("/editCategory/:parentCategoryId/:categoryId", {
            templateUrl: "./templates/edit-category.html",
            controller: "editCategoryController",
        });

        $routeProvider.when("/editPage/:categoryId/:pageId", {
            templateUrl: "./templates/edit-page.html",
            controller: "editPageController",
        });

        $routeProvider.when("/addPage/:categoryId", {
            templateUrl: "./templates/new-page.html",
            controller: "newPageController",
        });

        $routeProvider.when("/deletePage/:categoryId/:pageId", {
            templateUrl: "./templates/delete-page.html",
            controller: "deletePageController",
        });

        $routeProvider.otherwise({ redirectTo : "/list" });
    }]);
