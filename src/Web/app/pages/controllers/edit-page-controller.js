var tada = tada || {};
tada.pages = tada.pages || {};
tada.pages.controller("editPageController", ["$scope", "pageService", "$routeParams", "$location", "arrayUtils", function($scope, pageService, $routeParams, $location, arrayUtils) {
    "use strict";

    $scope.errors = [];
    $scope.categoryId = $routeParams.categoryId || null;
    $scope.editPageId = $routeParams.pageId;
    $scope.page = null;
    $scope.pageTemplates = [];
    $scope.pageCategories = [];
    $scope.articles = [];
    $scope.imageLists = [];

    pageService.getPage($scope.editPageId,
        function(result) {
            $scope.page = result;
        },
        function(err) {
            $scope.errors.push( "Could not load the page.");
        });

    pageService.getPageTemplates(
        function(result) {
            $scope.pageTemplates = result;
        },
        function(err) {
            $scope.errors.push( "Could not load the page templates.");
        });

    pageService.getPageCategories(
        function(result) {
            $scope.pageCategories = result;
        },
        function(err) {
            $scope.errors.push( "Could not load the page categories.");
        });

    pageService.getArticles(
        function(result) {
            $scope.articles = result;
        },
        function(err) {
            $scope.errors.push( "Could not load the articles.");
        });

    pageService.getImageLists(
        function(result) {
            $scope.imageLists = result;
        },
        function(err) {
            $scope.errors.push( "Could not load the image lists.");
        });

    $scope.selectedTemplate = function()
    {
        return arrayUtils.single($scope.pageTemplates, function(template) {
            return template.pageTemplateId === $scope.page.pageTemplateId;
        });
    };

    $scope.showPageCategorySelector = function(key) {
        return isViewOfType(key, "pageCategoryList");
    };

    $scope.showArticleSelector = function(key) {
        return isViewOfType(key, "article");
    };

    $scope.showImageListSelector = function(key) {
        return isViewOfType(key, "imageList");
    };

    $scope.showTextField = function(key) {
        return isViewOfType(key, "text");
    };

    function isViewOfType(key, type)
    {
        var template = $scope.selectedTemplate();
        var view = arrayUtils.single(template.views, function(v) {
            return v.key === key;
        });

        return view.type === type;
    }

    function findViewInTemplate(template, key)
    {
        var inTemplate = arrayUtils.single(template.views, function(view) {
            return view.key === key;
        });
        return inTemplate;
    }

    function findViewInPage(key)
    {
        var existing = arrayUtils.single($scope.page.views, function(view) {
            return view.key === key;
        });
        return existing;
    }

    $scope.$watch(
        function() {
            return $scope.page.pageTemplateId;
        },
        function(newId,oldId) {
            if( newId !== oldId ) {
                // Update our views to match that of the selected template
                var newTemplate = arrayUtils.single($scope.pageTemplates, function(template) {
                    return template.pageTemplateId === newId;
                });

                var i;
                for( i = 0; i < newTemplate.views.length; i++ ) {
                    var newView = newTemplate.views[i];

                    var existing = findViewInPage(newView.key);

                    if (!existing) {
                        $scope.page.views.push( {
                            key: newView.key,
                            valueId: null,
                            value: null
                        });
                    }
                }

                // Delete ones that aren't in the new template
                for ( i = $scope.page.views.length - 1; i >= 0; i--) {
                    var oldView = $scope.page.views[i];

                    // var inTemplate = arrayUtils.single(newTemplate.views, function(view) {
                    //     return view.key === oldView.key;
                    // });
                    var inTemplate = findViewInTemplate(newTemplate, oldView.key);

                    if ( !inTemplate ) {
                        $scope.page.views.splice(i, 1);
                    }
                }
            }
        }
    );

    $scope.editPage = function() {
        $scope.errors = [];

        var okToSave = true;
        if ( pageService.pageWithTitleExists($scope.categoryId, $scope.page.title, $scope.page.pageId)) {
            $scope.errors.push("The specified title is already in use in this category, please choose another.");
            okToSave = false;
        }

        if ( pageService.pageWithUrlExists($scope.page.url, $scope.page.pageId)) {
            $scope.errors.push("The specified url is already in use, please choose another.");
            okToSave = false;
        }

        if (!okToSave) {
            return false;
        }

        pageService.editPage($scope.page,
            function() {
                $location.path("/list/" + $scope.categoryId);
            },
            function(err) {
                $scope.errors.push( "Could not update the page.");
            });
    };
}]);

