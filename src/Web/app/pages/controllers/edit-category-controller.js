var tada = tada || {};
tada.pages = tada.pages || {};
tada.pages.controller("editCategoryController", ["$scope", "pageService", "$routeParams", "$location", function($scope, pageService, $routeParams, $location) {
    "use strict";

    $scope.errors = [];
    $scope.category = null;
    $scope.editCategoryId = $routeParams.categoryId;
    $scope.parentCategoryId = $routeParams.parentCategoryId || null;

    pageService.getCategory($scope.editCategoryId,
        function(result) {
            $scope.category = result;
        },
        function(err) {
            $scope.errors.push( "Could not load the category.");
        });

    $scope.editCategory = function() {
        // Validate the category name is unique for this parent
        $scope.errors = [];

        if (pageService.categoryWithNameExists($scope.parentCategoryId, $scope.category.pageCategoryName, $scope.editCategoryId)) {
            $scope.errors.push("The specified name is already in use, please choose another.");
            return false;
        }

        pageService.editCategory($scope.category,
            function() {
                $location.path("/list/" + $scope.category.pageCategoryId);
            },
            function(err) {
                $scope.errors.push( "Could not update the category.");
            });
    };
}]);

