var tada = tada || {};
tada.pages = tada.pages || {};
tada.pages.controller("newCategoryController", ["$scope", "pageService", "$routeParams", "$location", function($scope, pageService, $routeParams, $location) {
    "use strict";

    $scope.errors = [];
    $scope.categoryName = "";
    $scope.parentCategoryId = $routeParams.parentCategoryId || null;

    $scope.addCategory = function() {
        // Validate the category name is unique for this parent
        $scope.errors = [];

        if ( pageService.categoryWithNameExists($scope.parentCategoryId, $scope.categoryName, null)) {
            $scope.errors.push("The specified name is already in use, please choose another.");
            return false;
        }

        // create
        var category =  {
            pageCategoryId: null,
            pageCategoryName: $scope.categoryName,
            categories: [],
            pages: []
        };

        pageService.addCategory($scope.parentCategoryId, category,
            function(id) {
                $location.path("/list/" + id);
            },
            function(err) {
                $scope.errors.push("Could not add the category.");
            });
    };
}]);

