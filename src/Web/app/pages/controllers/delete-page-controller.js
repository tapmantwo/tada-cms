var tada = tada || {};
tada.pages = tada.pages || {};
tada.pages.controller("deletePageController", ["$scope", "pageService", "$routeParams", "$location", function($scope, pageService, $routeParams, $location) {
    "use strict";

    $scope.errors = [];
    $scope.pageId = $routeParams.pageId || null;
    $scope.categoryId = $routeParams.categoryId || null;
    $scope.page = null;

    pageService.getPage($scope.pageId,
        function(result) {
            $scope.page = result;
        },
        function(err) {
            $scope.errors.push("Could not load page.");
        });

    $scope.deletePage = function() {
        // Validate the category name is unique for this parent
        $scope.errors = [];

        pageService.deletePage($scope.pageId,
            function(id) {
                $location.path("/list/" + $scope.categoryId);
            },
            function(err) {
                $scope.errors.push("Could not delete the page.");
            });
    };
}]);

