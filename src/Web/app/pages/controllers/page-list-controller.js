var tada = tada || {};
tada.pages = tada.pages || {};
tada.pages.controller("pageListController", ["$scope", "pageService", "$routeParams", "arrayUtils", "$location", function($scope, pageService, $routeParams, arrayUtils, $location) {
    "use strict";

    $scope.currentCategory = null;
    $scope.categoryId = $routeParams.categoryId || null;
    $scope.showDeleted = false;
    $scope.deletedPageCount = 0;
    $scope.pageList = null;
    $scope.previousCategories = [];
    $scope.previousCategory = null;

    pageService.getPageList(function(result) {
        $scope.pageList = result;

        if ( $scope.categoryId ) {
            // Find the specified category
            var route = [];
            var category = findCategory($scope.categoryId, result, route);
            if ( category ) {
                $scope.previousCategory = route[0];
                $scope.previousCategories = arrayUtils.reverse(route);
                $scope.currentCategory = category;
            } else {
                $scope.previousCategories = [];
                $scope.currentCategory = result[0];
            }
        } else {
            $scope.previousCategories = [];
            $scope.currentCategory = result[0];
        }
        $scope.categoryId = $scope.currentCategory.pageCategoryId;

        var deletedCount = 0;
        for( var i = 0; i < $scope.currentCategory.pages.length; i++ ) {
            if ( $scope.currentCategory.pages[i].deleted ) {
                deletedCount++;
            }
        }

        $scope.deletedPageCount = deletedCount;
    });
    
    $scope.shouldShowPage = function(page)
    {
        return !page.deleted || $scope.showDeleted;
    };

    $scope.selectCategory = function(category)
    {
        if ( category.pageCategoryId ) {
            $location.path("/list/" + category.pageCategoryId);
        } else {
            $location.path("/list");
        }

    };

    $scope.selectPage = function(page)
    {
        if($scope.categoryId) {
            $location.path("/editPage/" + $scope.categoryId + "/" + page.pageId);
        } else {
            $location.path("/editPage//" + page.pageId);
        }
    };

    function findCategory(categoryId, categories, route)
    {
        var category = null;

        var i;
        for( i = 0; i < categories.length; i++ ) {
            if ( categories[i].pageCategoryId === categoryId ) {
                category = categories[i];
                break;
            }
        }

        if ( !category ) {
            // Check subcategories instead then
            for( i = 0; i < categories.length; i++ ) {
                var subCategory = findCategory(categoryId, categories[i].categories, route);
                if ( subCategory ) {
                    category = subCategory;
                    route.push(categories[i]);
                    break;
                }
            }
        }

        return category;
    }
}]);

