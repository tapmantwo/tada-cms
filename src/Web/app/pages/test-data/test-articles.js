var tada = tada || {};
tada.pages = tada.pages || {};
tada.pages.testArticles = [
        {
            articleCategoryId: null,
            articleCategoryName: null,
            articleId: "e0542205b29c4905980db3a765805716",
            name: "Home page"
        },
        {
            articleCategoryId: "4c1f44256fd14267be108cb045ea3c7c",
            articleCategoryName: "Toms Reviews",
            articleId: "c8b60d789508409cbf6ce7a2b2586300",
            name: "Manic Miner C64"
        },
        {
            articleCategoryId: "4c1f44256fd14267be108cb045ea3c7c",
            articleCategoryName: "Richards Reviews",
            articleId: "03823d5f66574e6b86c34ba418775955",
            name: "Manic Miner C64"
        },
        {
            articleCategoryId: "4c1f44256fd14267be108cb045ea3c7c",
            articleCategoryName: "Toms Reviews",
            articleId: "cb5f8eb21d1045879b76f5ac1001626c",
            name: "Short Circuit C64"
        },
        {
            articleCategoryId: "4c1f44256fd14267be108cb045ea3c7c",
            articleCategoryName: "Richards Reviews",
            articleId: "c26c2a80f721428bbf53b7211a84807a",
            name: "Short Circuit C64"
        },
        {
            articleCategoryId: "4c1f44256fd14267be108cb045ea3c7c",
            articleCategoryName: "Toms Reviews",
            articleId: "deda4f91e721471da9e6c09862b27f99",
            name: "Crystal Castles Atari 2600"
        },
        {
            articleCategoryId: "4c1f44256fd14267be108cb045ea3c7c",
            articleCategoryName: "Richards Reviews",
            articleId: "4ba6ceb62cc4497394c965656849c0e6",
            name: "Crystal Castles Atari 2600"
        },
        {
            articleCategoryId: "4c1f44256fd14267be108cb045ea3c7c",
            articleCategoryName: "Toms Reviews",
            articleId: "4bc9077e02d6455eb48e7c4c4e8a9086",
            name: "Yars Revenge Atari 2600"
        },
        {
            articleCategoryId: "4c1f44256fd14267be108cb045ea3c7c",
            articleCategoryName: "Richards Reviews",
            articleId: "8f1c8b91f85e4e9dbc15e1a0a0afeb95",
            name: "Yars Revenge Atari 2600"
        },
    ];