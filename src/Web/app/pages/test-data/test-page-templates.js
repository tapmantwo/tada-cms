var tada = tada || {};
tada.pages = tada.pages || {};
tada.pages.testPageTemplates = [
        {
            pageTemplateId: "7b30eac48df646959297aa21718e93a8",
            name: "Home Template",
            views: [
                {
                    key: "article1",
                    type: "article",
                }
            ]
        },
        {
            pageTemplateId: "8b17b26194c240fb9e6b5bd16f0c6193",
            name: "Review List Template",
            views: [
                {
                    key: "pageCategory1",
                    type: "pageCategoryList",
                }
            ]
        },
        {
            pageTemplateId: "dad8dde4-45bb-426d-8c85-c10e33d267a7",
            name: "Review Template",
            views: [
                {
                    key: "articleTom",
                    type: "article",
                },
                {
                    key: "articleRichard",
                    type: "article",
                },
                {
                    key: "screenshots",
                    type: "imageList",
                },
                {
                    key: "score",
                    type: "text",
                }
            ]
        }
    ];
