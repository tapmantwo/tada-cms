var tada = tada || {};
tada.pages = tada.pages || {};
tada.pages.testPageList = [
		{
			pageCategoryId: null,
			pageCategoryName: "Root",
			categories: [
				{
					pageCategoryId: "8317eb63a2e2449883cadb4e91423b9b",
					pageCategoryName: "General",
					categories: [],
					pages: [
						{
							pageId: "af7d3c9ab6f84e3da3c2ada01b6d7f9b",
							pageTitle: "Home",
							deleted: false
						}
					]
				},
				{
					pageCategoryId: "c956ba4006dd436389db2428a8225f3e",
					pageCategoryName: "Reviews",
					categories: [
						{
							pageCategoryId: "6fac0b2e9b9b4572b333d534328bac02",
							pageCategoryName: "C64",
							categories: [],
							pages: [
								{
									pageId: "6a48cc61a6864e98b6dc10b15a783b12",
									pageTitle: "Manic Miner | C64",
									deleted: false
								}
							]
						},
						{
							pageCategoryId: "6bf1caa74fe14fd8a9ea5d2cb099d9fb",
							pageCategoryName: "Atari 2600",
							categories: [],
							pages: [
								{
									pageId: "ccb8a8cf4cee4ed9ba9511c690d5b606",
									pageTitle: "Crystal Castles | Atari 2600",
									deleted: false
								}
							]
						}
					],
					pages: []
				}
			],
			pages: [
				{
					pageId: "84ba2a4671754fc09361952762448b79",
					pageTitle: "Reviews",
					pageUrl: "/reviews",
					deleted: false
				}
			]
		}
	];