var tada = tada || {};
tada.pages = tada.pages || {};
tada.pages.testPageCategories = [
        {
            parentCategoryId: null,
            parentCategoryName: null,
            pageCategoryId: "8317eb63a2e2449883cadb4e91423b9b",
            name: "General",
        },
        {
            parentCategoryId: null,
            parentCategoryName: null,
            pageCategoryId: "c956ba4006dd436389db2428a8225f3e",
            name: "Reviews",
        },
        {
            parentCategoryId: "c956ba4006dd436389db2428a8225f3e",
            parentCategoryName: "Reviews",
            pageCategoryId: "6fac0b2e9b9b4572b333d534328bac02",
            name: "C64",
        },
        {
            parentCategoryId: "c956ba4006dd436389db2428a8225f3e",
            parentCategoryName: "Reviews",
            pageCategoryId: "6bf1caa74fe14fd8a9ea5d2cb099d9fb",
            name: "Atari 2600",
        }
    ];