var tada = tada || {};
tada.pages = tada.pages || {};
tada.pages.testPages = [
        {
            pageId: "af7d3c9ab6f84e3da3c2ada01b6d7f9b",
            title: "Home",
            url: "/home",
            metaDescription: "The home page",
            metaKeywords: "test home page",
            pageTemplateId: "7b30eac48df646959297aa21718e93a8",
            views: [
                {
                    key: "article1",
                    valueId: "e0542205b29c4905980db3a765805716"
                }
            ],
            deleted: false
        },
        {
            pageId: "84ba2a4671754fc09361952762448b79",
            title: "Reviews",
            url: "/reviews",
            metaDescription: "A list of reviews",
            metaKeywords: "reviews",
            pageTemplateId: "8b17b26194c240fb9e6b5bd16f0c6193",
            views: [
                {
                    key: "pageCategory1",
                    valueId: "c956ba4006dd436389db2428a8225f3e"
                }
            ],
            deleted: false
        },
        {
            pageId: "9561594288074c0b8ac642c092ec3256",
            title: "Manic Miner | C64",
            url: "/reviews/c64/manic-miner",
            metaDescription: "C64 manic miner review",
            metaKeywords: "C64 manic miner review retro",
            pageTemplateId: "dad8dde4-45bb-426d-8c85-c10e33d267a7",
            views: [
                {
                    key: "articleTom",
                    valueId: "c8b60d789508409cbf6ce7a2b2586300"
                },
                {
                    key: "articleRichard",
                    valueId: "03823d5f66574e6b86c34ba418775955"
                },
                {
                    key: "screenshots",
                    valueId: "813a4a4da0784513b1dbba5fe33562ef"
                },
                {
                    key: "score",
                    value: "89"
                }
            ],
            deleted: false
        },
        {
            pageId: "ccb8a8cf4cee4ed9ba9511c690d5b606",
            title: "Crystal Castles | Atari 2600",
            url: "/reviews/atari-2600/crystal-castles",
            metaDescription: "Atari 2600 Crystal Castles review",
            metaKeywords: "Atari 2600 Crystal Castles review retro",
            pageTemplateId: "dad8dde4-45bb-426d-8c85-c10e33d267a7",
            views: [
                {
                    key: "articleTom",
                    valueId: "deda4f91e721471da9e6c09862b27f99"
                },
                {
                    key: "articleRichard",
                    valueId: "4ba6ceb62cc4497394c965656849c0e6"
                },
                {
                    key: "screenshots",
                    valueId: "a9dc7a151ffc4b4d94d8b856821431ad"
                },
                {
                    key: "score",
                    value: "82"
                }
            ],
            deleted: false
        },
    ];