var tada = tada || {};
tada.pages = tada.pages || {};
tada.pages.testImageLists = [
        {
            imageListCategoryId: null,
            imageListCategoryName: "C64",
            imageListId: "813a4a4da0784513b1dbba5fe33562ef",
            name: "Manic Miner"
        },
        {
            imageListCategoryId: null,
            imageListCategoryName: "Atari 2600",
            imageListId: "a9dc7a151ffc4b4d94d8b856821431ad",
            name: "Crystal Castles"
        },
        {
            imageListCategoryId: null,
            imageListCategoryName: "Atari 2600",
            imageListId: "bd7ecc9a9a9145ba968df48a39e557c7",
            name: "Yars Revenge"
        },
        {
            imageListCategoryId: null,
            imageListCategoryName: "C64",
            imageListId: "aac6c7dd204b4c3b819532130a3f1f1a",
            name: "Short Circuit"
        }
    ];