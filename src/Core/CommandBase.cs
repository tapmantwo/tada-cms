﻿namespace Tada.Cms.Core
{
    using System;
    using System.Data;

    public class CommandBase
    {
        private readonly IDbConnection connection;

        private bool ownTheConnection;

        protected CommandBase(IDbConnection connection)
        {
            this.connection = connection;
            this.ownTheConnection = false;
        }

        protected void InvokeExecute(Action<IDbConnection> execute)
        {
            try
            {
                if (this.connection.State != ConnectionState.Open)
                {
                    this.connection.Open();
                    this.ownTheConnection = true;
                }

                execute.Invoke(this.connection);
            }
            finally
            {
                if (this.ownTheConnection && this.connection.State != ConnectionState.Closed)
                {
                    this.connection.Close();
                }
            }
        }
    }
}