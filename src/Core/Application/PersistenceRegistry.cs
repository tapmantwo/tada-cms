﻿namespace Tada.Cms.Core.Application
{
    using System.Data;

    using ServiceStack.OrmLite;

    using StructureMap.Configuration.DSL;

    public class PersistenceRegistry : Registry
    {
        public PersistenceRegistry(string connectionString)
        {
            this.For<IDbConnection>().HybridHttpOrThreadLocalScoped()
                .Use(
                    c =>
                    {
                        OrmLiteConfig.DialectProvider = SqlServerDialect.Provider;

                        return connectionString.ToDbConnection();
                    });
        }
    }
}