﻿namespace Tada.Cms.Core
{
    using System;
    using System.Data;

    public abstract class QueryBase
    {
        private readonly IDbConnection connection;

        private bool ownTheConnection;

        protected QueryBase(IDbConnection connection)
        {
            this.connection = connection;
            this.ownTheConnection = false;
        }

        protected TModel Execute<TModel>(Func<IDbConnection, TModel> func)
        {
            try
            {
                if (this.connection.State != ConnectionState.Open)
                {
                    this.connection.Open();
                    this.ownTheConnection = true;
                }

                return func.Invoke(this.connection);
            }
            finally
            {
                if (this.ownTheConnection && this.connection.State != ConnectionState.Closed)
                {
                    this.connection.Close();
                }
            }
        }
    }
}