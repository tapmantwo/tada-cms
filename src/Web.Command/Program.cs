﻿namespace Tada.Cms.Web.Command
{
    using System;
    using System.Configuration;
    using System.Diagnostics;

    using Microsoft.Owin.Hosting;

    using Tada.Cms.Web.Command.Application;

    using Topshelf;

    public class Program
    {
        private static readonly Type[] EnforceReferencesFor =
            {
                typeof(Microsoft.Owin.Host.HttpListener.OwinServerFactory),
                typeof(Newtonsoft.Json.ConstructorHandling)
            };

        private static void Main(string[] args)
        {
            string hostname = ConfigurationManager.AppSettings["hostname"] ?? "localhost";

            int port;

            if (!int.TryParse(ConfigurationManager.AppSettings["port"], out port))
            {
                throw new Exception("Couldn't find port setting in appSettings.config.");
            }

            var scheme = string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["scheme"])
               ? "http" : ConfigurationManager.AppSettings["scheme"].ToLower();

            var path = string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["path"])
                ? string.Empty : string.Format("/{0}", ConfigurationManager.AppSettings["path"].ToLower().Trim('/'));

            var url = string.Format("{0}://{1}:{2}{3}", scheme, hostname, port, path);

            HostFactory.Run(
                ts =>
                {
                    ts.Service<StartOptions>(
                        s =>
                        {
                            IDisposable owinHost = null;

                            s.ConstructUsing(
                                settings => new StartOptions(url) { Port = port });

                            s.WhenStarted(
                                options =>
                                {
                                    owinHost = WebApp.Start<Startup>(options);

                                    Trace.WriteLine(
                                        string.Format("Listening on {0}", url));
                                });

                            s.WhenStopped(
                                callback =>
                                {
                                    if (owinHost != null)
                                    {
                                        owinHost.Dispose();
                                    }
                                });
                        });

                    ts.RunAsLocalSystem();
                    ts.SetDisplayName("[Tada Cms] Web Commands");
                    ts.SetServiceName("tada-cms-web-command");
                });
        }
    }
}