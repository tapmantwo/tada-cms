﻿namespace Tada.Cms.Web.Command.Application
{
    using System.Configuration;
    using System.Web.Http;

    using StructureMap;

    using Tada.Cms.Core.Application;

    public static class StructureMapSetup
    {
        public static void Configure()
        {
            ObjectFactory.Configure(
                cfg =>
                    {
                        var connectionSetting = ConfigurationManager.ConnectionStrings["tada-cms"];
                        var connectionString = connectionSetting != null ? connectionSetting.ConnectionString : null;

                        cfg.IncludeRegistry(new PersistenceRegistry(connectionString));

                        cfg.Scan(
                            scanner =>
                            {
                                scanner.AssemblyContainingType<System.Web.Http.Hosting.IHostBufferPolicySelector>();
                                scanner.Assembly("Tada.Cms.Web.Command");
                                scanner.WithDefaultConventions();
                            });
                    });
        }
    }
}