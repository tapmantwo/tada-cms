﻿namespace Tada.Cms.Web.Query.Endpoints
{
    using System.Collections.Generic;

    using Tada.Cms.Web.Query.Application;
    using Tada.Cms.Web.Query.Behaviors;

    using Simple.Web;
    using Simple.Web.Behaviors;
    using Simple.Web.Links;

    [UriTemplate("/")]
    public class GetEndpoint : IApexRequest, IGet, IOutput<IEnumerable<Link>>
    {
        public IApexContext ApexContext { get; set; }

        public IEnumerable<Link> Output { get; private set; }

        public Status Get()
        {
            this.Output = LinkHelper.GetRootLinks();

            return 200;
        }
    }
}