﻿namespace DataCreator
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Reflection;

    using DomainData;

    using ServiceStack.DataAnnotations;
    using ServiceStack.OrmLite;
    using ServiceStack.OrmLite.SqlServer;

    using Tada.Cms.Domain;
    using Tada.Cms.Domain.Entities;

    public class Program
    {
        private static readonly Func<string, string> SqlDatabaseName = s => GetConnectionValue(s, "Database");

        private static readonly Func<Type, string> TableName = entity => Attribute.IsDefined(entity, typeof(ServiceStack.DataAnnotations.AliasAttribute)) ?
                    ((AliasAttribute)Attribute.GetCustomAttribute(entity, typeof(AliasAttribute))).Name :
                    entity.Name;

        public static readonly Func<Type, bool> IsEntityType = t => t.Name.ToLower() != "entity" && t.Namespace != null && (t.Namespace.Contains(".Entities") && !t.IsAbstract && t.IsClass);

        public static readonly Func<Type, bool> IsDataType = t => t.Name.EndsWith("Data");

        private static string connectionString;

        private static IEnumerable<Type> scanAssemblyTypes;

        private static string entityNamespace;

        public static void Main(string[] args)
        {
            ProcessCommandLineArgs(args);

            scanAssemblyTypes = typeof(Article).Assembly.GetExportedTypes().Union(typeof(ArticleData).Assembly.GetExportedTypes());
            
            string databaseName;
            IDbConnection connection;

            OrmLiteConfig.DialectProvider = SqlServerDialect.Provider;
            (OrmLiteConfig.DialectProvider as SqlServerOrmLiteDialectProvider).UseDatetime2(true);

            databaseName = SqlDatabaseName(connectionString);
            connection = connectionString.Replace(databaseName, "master").ToDbConnection();

            connection.Open();

            var databaseExists = connection.ScalarFmt<int>("SELECT COUNT(*) FROM sys.databases WHERE [name] = {0}", databaseName) != 0;

            if (databaseExists)
            {
                Console.WriteLine("Dropping database '" + databaseName + "'");

                connection.ExecuteSql("ALTER DATABASE [" + databaseName + "] SET SINGLE_USER WITH ROLLBACK IMMEDIATE;");
                connection.ExecuteSql("DROP DATABASE [" + databaseName + "]");
            }

            Console.WriteLine("Creating database '" + databaseName + "'");

            connection.ExecuteSql("CREATE DATABASE [" + databaseName + "]");

            connection.ChangeDatabase(databaseName);

            try
            {
                var types = scanAssemblyTypes.Where(IsEntityType).ToArray();

                if (!types.Any())
                {
                    Console.WriteLine("Unable to locate any entities in specified assembly.");
                    Environment.Exit(0);
                }

                var dataTypes = scanAssemblyTypes.Where(IsDataType).ToArray();

                if (string.IsNullOrWhiteSpace(entityNamespace))
                {
                    if (types.Select(x => x.AssemblyQualifiedName.Replace("." + x.Name, string.Empty)).Distinct().Count() == 1)
                    {
                        entityNamespace = types.ElementAt(0).Namespace;
                    }

                    if (string.IsNullOrWhiteSpace(entityNamespace))
                    {
                        throw new Exception("Unable to determine entity namespace.");
                    }
                    else
                    {
                        Console.WriteLine("Determined entity namespace as '{0}'", entityNamespace);
                    }
                }

                var entities = DeriveEntityOrder(types.Where(x => x.Namespace.StartsWith(entityNamespace))).ToArray();

                if (!entities.Any())
                {
                    throw new Exception("No qualifying entities could be found. Check namespace!");
                }

                using (var trans = connection.OpenTransaction(IsolationLevel.Serializable))
                {
                    try
                    {
                        foreach (var entity in entities)
                        {
                            var tableName = TableName(entity);

                            if (!connection.TableExists(tableName))
                            {
                                Console.WriteLine("  - Creating table: " + tableName);
                                connection.CreateTable(false, entity);
                            }
                        }

                        foreach (var entity in entities)
                        {
                            Console.WriteLine("Processing entity: " + entity.FullName);

                            var dataType = dataTypes.FirstOrDefault(x => x.Name.Equals(entity.Name + "Data"));

                            if (dataType != null)
                            {
                                Console.WriteLine("  - Inserting data from: " + dataType.FullName);

                                var data = (IEnumerable<object>)Activator.CreateInstance(dataType);

                                var method =
                                    typeof(OrmLiteWriteConnectionExtensions).GetMethod(
                                        "SaveAll",
                                        BindingFlags.Static | BindingFlags.Public).MakeGenericMethod(entity);

                                method.Invoke(null, new object[] { trans.Connection, data });
                            }
                            else
                            {
                                Console.WriteLine("  - No data found for: " + entity.Name + "Data");
                            }
                        }

                        trans.Commit();
                    }
                    catch
                    {
                        trans.Rollback();
                        throw;
                    }
                    finally
                    {
                        trans.Dispose();
                    }
                }
            }
            finally
            {
                connection.Close();
                connection.Dispose();
            }
        }

        public static IEnumerable<Type> DeriveEntityOrder(IEnumerable<Type> types)
        {
            var entities = types.ToDictionary(type => type, type => false);
            var list = new List<Type>(entities.Count);
            var pos = 0;

            while (entities.Any(x => !x.Value))
            {
                var entity = entities.ElementAt(pos);

                pos++;

                if (pos >= entities.Count)
                {
                    pos = 0;
                }

                if (entity.Value)
                {
                    continue;
                }

                var references =
                    entity.Key.GetProperties(BindingFlags.Public | BindingFlags.Instance)
                        .Where(x => Attribute.IsDefined(x, typeof(ServiceStack.DataAnnotations.ReferencesAttribute)))
                        .Select(x => Attribute.GetCustomAttribute(x, typeof(ServiceStack.DataAnnotations.ReferencesAttribute)))
                        .Cast<ReferencesAttribute>()
                        .Select(x => x.Type);

                if (references.Any())
                {
                    if (references.All(list.Contains))
                    {
                        for (var v = 0; v < list.Count; v++)
                        {
                            if (references.Any(x => x == list.ElementAt(v)))
                            {
                                list.Insert(v, entity.Key);
                                break;
                            }
                        }

                        entities[entity.Key] = true;
                    }
                }
                else
                {
                    entities[entity.Key] = true;
                    list.Add(entity.Key);
                }
            }

            list.Reverse();

            return list.ToArray();
        }

        public static string GetConnectionValue(string thisConnectionString, string key)
        {
            var pos = thisConnectionString.IndexOf(key, StringComparison.InvariantCultureIgnoreCase);

            if (pos == -1)
            {
                return string.Empty;
            }

            var endPos = thisConnectionString.IndexOf(";", pos + 1, StringComparison.InvariantCultureIgnoreCase);

            return endPos == -1 ? thisConnectionString.Substring(pos + key.Length + 1) : thisConnectionString.Substring(pos + key.Length + 1, endPos - (pos + key.Length + 1));
        }

        private static void ProcessCommandLineArgs(string[] args)
        {
            for (var x = 0; x < args.Length; x++)
            {
                switch (args[x].ToLower())
                {
                    case "/connectionstring":
                        connectionString = ParseValue(args, x++);
                        Console.WriteLine("* Connection string: {0}", connectionString);
                        break;

                    default:
                        throw new Exception("Unknown argument: " + args[x]);
                }
            }

            if (string.IsNullOrWhiteSpace(connectionString))
            {
                throw new Exception("You must specify a connection string.");
            }

            Console.WriteLine(string.Empty);
        }

        private static string ParseValue(string[] args, int pos)
        {
            if (pos + 1 > args.Length)
            {
                throw new Exception("No value specified for argument: " + args[pos]);
            }

            return args[pos + 1].Trim('"', ' ');
        }
    }
}
