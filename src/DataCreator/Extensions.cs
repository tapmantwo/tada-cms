namespace DataCreator
{
    using System.Collections;
    using System.Collections.Generic;

    public static class Extensions
    {
        public static TColl ToTypedCollection<TColl, T>(this IEnumerable ien)
            where TColl : IList<T>, new()
        {
            var collection = new TColl();

            foreach (var item in ien)
            {
                collection.Add((T)item);
            }

            return collection;
        }
    }
}