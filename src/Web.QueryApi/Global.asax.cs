﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;

namespace Tada.Cms.Web.QueryApi
{
    using global::Web.QueryApi;

    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configuration.DependencyResolver = new StructureMapResolver();
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}
