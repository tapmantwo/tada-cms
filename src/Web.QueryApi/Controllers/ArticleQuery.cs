﻿namespace Tada.Cms.Web.Query.Endpoints.Article
{
    using System;
    using System.Data;


    public class ArticleQuery : IArticleQuery
    {
        private readonly IDbConnection connection;

        public ArticleQuery(IDbConnection connection)
        {
            this.connection = connection;
        }

        public ArticleModel Execute(Guid articleId)
        {
            return new ArticleModel() { Title = "Whoo" };
            //return this.connection.SingleFmt<ArticleModel>("SELECT [Id] AS [ArticleId], [Content], [Summary], [Title] FROM [Article] WHERE [Id] = {0}", articleId);
        }
    }
}