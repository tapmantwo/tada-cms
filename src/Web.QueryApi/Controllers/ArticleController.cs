﻿namespace Web.QueryApi.Controllers
{
    using System;
    using System.Web.Http;
    using Tada.Cms.Web.Query.Endpoints.Article;

    public class ArticleController : ApiController
    {
        private readonly IArticleQuery query;

        public ArticleController(IArticleQuery query)
        {
            this.query = query;
        }

        public ArticleModel GetArticle(Guid id)
        {
            return this.query.Execute(id);
        }
    }
}
