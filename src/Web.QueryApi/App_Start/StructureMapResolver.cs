﻿namespace Web.QueryApi
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Web.Http.Dependencies;
    using System.Web.Services.Description;

    using StructureMap;

    using Tada.Cms.Core.Application;

    public class StructureMapResolver : IDependencyResolver
    {

        public StructureMapResolver()
        {
            ObjectFactory.Configure(this.Configure);
        }

        private void Configure(ConfigurationExpression cfg)
        {
            var connectionSetting = ConfigurationManager.ConnectionStrings["tada-cms"];
            var connectionString = connectionSetting != null ? connectionSetting.ConnectionString : null;

            cfg.IncludeRegistry(new PersistenceRegistry(connectionString));

            cfg.Scan(
                scanner =>
                {
                    scanner.Assembly(this.GetType().Assembly);
                    scanner.WithDefaultConventions();
                });
        }

        public IDependencyScope BeginScope()
        {
            return this;
        }

        public object GetService(Type serviceType)
        {
            if (serviceType.IsAbstract || serviceType.IsInterface)
                return ObjectFactory.TryGetInstance(serviceType);
            else
                return ObjectFactory.GetInstance(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return ObjectFactory.GetAllInstances(serviceType) as IEnumerable<object>;
        }

        public void Dispose()
        {
        }
    }
}