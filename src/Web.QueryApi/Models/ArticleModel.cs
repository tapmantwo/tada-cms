﻿namespace Tada.Cms.Web.Query.Endpoints.Article
{
    using System;

    public class ArticleModel
    {
        public Guid ArticleId { get; set; }

        public string Title { get; set; }

        public string Summary { get; set; }

        public string Content { get; set; }
    }
}