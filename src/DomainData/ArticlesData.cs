﻿namespace DomainData
{
    using System;
    using System.Collections;
    using System.Collections.Generic;

    using Tada.Cms.Domain.Entities;

    public class ArticleData : IEnumerable<Article>
    {
        public static IEnumerable<Article> Data = new[]
                                           {
                                               new Article
                                                   {
                                                       Id = new Guid("561F3414-7E6D-43F8-8FB0-375BF86AFB56"),
                                                       CategoryId = ArticleCategoryData.GeneralCategory.Id,
                                                       Content = "<strong>Welcome</strong>",
                                                       Deleted = false,
                                                       Summary = "The home page",
                                                       Title = "Home"
                                                   },
                                               new Article
                                                   {
                                                       Id = new Guid("EEB2996F-65C3-49FA-AE0A-29843D5BD9F3"),
                                                       CategoryId = ArticleCategoryData.TomC64Category.Id,
                                                       Content = "<strong>I love it</strong>",
                                                       Deleted = false,
                                                       Summary = "Tom reviews Short Circuit",
                                                       Title = "Short Circuit"
                                                   },
                                               new Article
                                                   {
                                                       Id = new Guid("6B3A4DA5-F755-48AB-92EC-EE592EA69CF6"),
                                                       CategoryId = ArticleCategoryData.RichC64Category.Id,
                                                       Content = "<strong>Buggy</strong>",
                                                       Deleted = false,
                                                       Summary = "Richard reviews Short Circuit",
                                                       Title = "Short Circuit"
                                                   }
                                           };

        public IEnumerator<Article> GetEnumerator()
        {
            return Data.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}