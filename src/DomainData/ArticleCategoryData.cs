﻿namespace DomainData
{
    using System;
    using System.Collections;
    using System.Collections.Generic;

    using Tada.Cms.Domain.Entities;

    public class ArticleCategoryData : IEnumerable<ArticleCategory>
    {
        public static ArticleCategory ReviewsCategory = new ArticleCategory { Id = new Guid("0DE0C0ED-FECC-4357-A41D-633D09F7282A"), Name = "Reviews", ParentId = null };
        public static ArticleCategory GeneralCategory = new ArticleCategory { Id = new Guid("734EB561-34DE-4A4C-B161-45004F54430E"), Name = "General", ParentId = null };

        public static ArticleCategory C64Category = new ArticleCategory
                                                        {
                                                            Id = new Guid("69CBE28F-042D-485A-813E-A83A391FFDB9"),
                                                            Name = "C64",
                                                            ParentId = new Guid("0DE0C0ED-FECC-4357-A41D-633D09F7282A")
                                                        };

        public static ArticleCategory RichC64Category = new ArticleCategory
        {
            Id = new Guid("8327B6F7-1A73-4687-B6EF-4B0D233499EA"),
            Name = "Richard",
            ParentId = new Guid("69CBE28F-042D-485A-813E-A83A391FFDB9")
        };

        public static ArticleCategory TomC64Category = new ArticleCategory
        {
            Id = new Guid("4784E13A-5A67-451E-AD9B-EB931FC0735E"),
            Name = "Tom",
            ParentId = new Guid("69CBE28F-042D-485A-813E-A83A391FFDB9")
        };

        public static ArticleCategory AtariCategory = new ArticleCategory
        {
            Id = new Guid("E7F32E0C-CA39-4B41-A540-FA40F93F195B"),
            Name = "Atari 2600",
            ParentId = new Guid("0DE0C0ED-FECC-4357-A41D-633D09F7282A")
        };


        public static IEnumerable<ArticleCategory>  Data = new[] { ReviewsCategory, GeneralCategory, C64Category, RichC64Category, TomC64Category, AtariCategory };

        public IEnumerator<ArticleCategory> GetEnumerator()
        {
            return Data.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
