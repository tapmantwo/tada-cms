﻿namespace Tada.Cms.Web.Query.Tests.Integration
{
    using System;
    using System.Data;
    using System.IO;
    using System.Reflection;

    using ServiceStack.OrmLite;

    public class IntegrationBase
    {
        private static readonly string OutputFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

        private readonly string databasePath;

        private readonly string databaseFilename;

        protected const int Port = 56001;

        protected readonly string ConnectionString;

        protected readonly IDbConnection Connection;

        public IntegrationBase()
        {
            var databaseName = "Db_" + Guid.NewGuid();

            this.databasePath = Path.Combine(OutputFolder, "Data_" + Guid.NewGuid());
            this.databaseFilename = Path.Combine(this.databasePath, databaseName);

            this.ConnectionString = string.Format("Data Source=|DataDirectory|\\{0}.vdb5;", databaseName);

            this.Connection = this.GetConnection();
        }

        public IDbConnection GetConnection()
        {
            AppDomain.CurrentDomain.SetData("DataDirectory", this.databasePath);

            Directory.CreateDirectory(this.databasePath);

            OrmLiteConfig.DialectProvider = VistaDBDialect.Provider;

            var connection = string.Format(this.ConnectionString).ToDbConnection();

            connection.ExecuteSql("CREATE DATABASE '" + this.databaseFilename + "', PAGE SIZE 4, LCID 1033, CASE SENSITIVE FALSE;");

            return connection;
        }

        public void Dispose()
        {
            if (this.Connection != null)
            {
                this.Connection.Close();
                this.Connection.Dispose();
            }

            File.Delete(this.databaseFilename);
        }
    }
}