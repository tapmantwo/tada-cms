﻿namespace Tada.Cms.Domain.Entities
{
    using System;
    using System.Collections.Generic;

    public class PageTemplate
    {
        public enum ViewType
        {
            Article,
            ImageList,
            PageCategoryList,
            Text
        }

        public Guid Id { get; set; }

        public string Name { get; set; }

        public IDictionary<string, ViewType> Type { get; set; }
    }
}