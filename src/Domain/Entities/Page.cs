﻿namespace Tada.Cms.Domain.Entities
{
    using System;
    using System.Collections.Generic;

    public class Page
    {
        public Guid Id { get; set; }

        public Guid CategoryId { get; set; }

        public string Title { get; set; }

        public string Url { get; set; }

        public string MetaDescription { get; set; }

        public string MetaKeywords { get; set; }

        public Guid PageTemplateId { get; set; }

        public Dictionary<string, string> Views { get; set; }

        public bool Deleted { get; set; }
    }
}