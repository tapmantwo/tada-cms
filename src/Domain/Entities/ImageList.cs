﻿namespace Tada.Cms.Domain.Entities
{
    using System;
    using System.Collections.Generic;

    public class ImageList
    {
        public Guid Id { get; set; }

        public Guid CategoryId { get; set; }

        public string Name { get; set; }

        public IEnumerable<Image> Images { get; set; }
    }
}