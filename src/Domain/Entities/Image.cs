﻿namespace Tada.Cms.Domain.Entities
{
    using System;

    public class Image
    {
        public Guid Id { get; set; }

        public string Url { get; set; }

        public string Caption { get; set; }
    }
}