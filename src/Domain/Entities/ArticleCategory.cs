﻿namespace Tada.Cms.Domain.Entities
{
    using System;

    public class ArticleCategory
    {
        public Guid Id { get; set; }

        public Guid? ParentId { get; set; }

        public string Name { get; set; }
    }
}