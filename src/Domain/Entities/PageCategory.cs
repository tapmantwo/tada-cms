﻿namespace Tada.Cms.Domain.Entities
{
    using System;

    public class PageCategory
    {
        public Guid Id { get; set; }

        public Guid? ParentId { get; set; }

        public string Name { get; set; }
    }
}