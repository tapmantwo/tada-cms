﻿namespace Tada.Cms.Domain.Entities
{
    using System;

    public class Article
    {
        public Guid Id { get; set; }

        public Guid CategoryId { get; set; }

        public string Title { get; set; }

        public string Summary { get; set; }

        public string Content { get; set; }

        public bool Deleted { get; set; }
    }
}