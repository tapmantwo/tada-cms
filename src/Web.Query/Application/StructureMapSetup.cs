﻿namespace Tada.Cms.Web.Query.Application
{
    using System.Configuration;

    using StructureMap;

    using Tada.Cms.Core.Application;

    public static class StructureMapSetup
    {
        public static void Configure()
        {
            ObjectFactory.Configure(
                cfg =>
                {
                    var connectionSetting = ConfigurationManager.ConnectionStrings["tada-cms"];
                    var connectionString = connectionSetting != null ? connectionSetting.ConnectionString : null;

                    cfg.IncludeRegistry(new PersistenceRegistry(connectionString));

                    cfg.Scan(
                        scanner =>
                        {
                            scanner.Assembly("Tada.Cms.Web.Query");
                            scanner.WithDefaultConventions();
                        });
                });
        }
    }
}