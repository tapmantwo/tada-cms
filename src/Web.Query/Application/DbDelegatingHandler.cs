﻿namespace Tada.Cms.Web.Query.Application
{
    using System.Data;
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;

    using StructureMap;

    public class DbDelegatingHandler : DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var connection = ObjectFactory.GetInstance<IDbConnection>();
            try
            {
                connection.Open();
                return base.SendAsync(request, cancellationToken);
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
