﻿namespace Tada.Cms.Web.Query.Endpoints.ArticleCategoryList
{
    using System;
    using System.Collections.Generic;

    public class ArticleCategoryListModel
    {
        public Guid ArticleCategoryId { get; set; }

        public string ArticleCategoryName { get; set; }

        public IEnumerable<ArticleCategoryListModel> Categories { get; set; }

        public IEnumerable<ArticleListItemModel> Articles { get; set; }
    }
}