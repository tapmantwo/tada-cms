﻿namespace Tada.Cms.Web.Query.Endpoints.ArticleCategoryList
{
    using System;

    public class ArticleListItemModel
    {
        public Guid ArticleId { get; set; }

        public string ArticleTitle { get; set; }
    }
}