﻿namespace Tada.Cms.Web.Query.Endpoints.ArticleCategoryList
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;

    using ServiceStack.OrmLite;

    public class ArticleCategoryListQuery : IArticleCategoryListQuery
    {
        private readonly IDbConnection connection;

        public ArticleCategoryListQuery(IDbConnection connection)
        {
            this.connection = connection;
        }

        public IEnumerable<ArticleCategoryListModel> Execute()
        {
            var allCategories = this.connection.Select<ArticleCategoryHeader>("SELECT [Id], [Name], [ParentId] FROM [ArticleCategory]");

            var allArticles = this.connection.Select<ArticleHeader>("SELECT [Id], [Title], [CategoryId] FROM [Article] WHERE [Deleted] = 0");

            // Build these into the nice tree structure for the front end
            var categoryTree =
                allCategories.Where(ac => ac.ParentId == null)
                    .Select(
                        ac =>
                        new ArticleCategoryListModel
                            {
                                ArticleCategoryId = ac.Id,
                                ArticleCategoryName = ac.Name,
                                Articles = allArticles.Where(a => a.CategoryId == ac.Id).Select(a => new ArticleListItemModel { ArticleId = a.Id, ArticleTitle = a.Title }),
                                Categories = this.BuildTree(ac.Id, allCategories, allArticles)
                            });

            return categoryTree;
        }

        private IEnumerable<ArticleCategoryListModel> BuildTree(Guid categoryId, List<ArticleCategoryHeader> allCategories, List<ArticleHeader> allArticles)
        {
            return this.BuildTreeItem(categoryId, allCategories, allArticles);
        }

        private IEnumerable<ArticleCategoryListModel> BuildTreeItem(Guid categoryId, IEnumerable<ArticleCategoryHeader> categories, IEnumerable<ArticleHeader> articles)
        {
            return categories.Where(ac => ac.ParentId == categoryId).Select(ac => new ArticleCategoryListModel
            {
                ArticleCategoryId = ac.Id,
                ArticleCategoryName = ac.Name,
                Articles = articles.Where(a => a.CategoryId == ac.Id).Select(a => new ArticleListItemModel
                {
                    ArticleId = a.Id,
                    ArticleTitle = a.Title
                }),
                Categories = this.BuildTreeItem(ac.Id, categories, articles)
            });
        }

        public class ArticleCategoryHeader
        {
            public Guid Id { get; set; }

            public string Name { get; set; }

            public Guid? ParentId { get; set; }
        }

        public class ArticleHeader
        {
            public Guid Id { get; set; }

            public string Title { get; set; }

            public Guid CategoryId { get; set; }
        }
    }
}