namespace Tada.Cms.Web.Query.Endpoints.ArticleCategoryList
{
    using System.Collections.Generic;

    public interface IArticleCategoryListQuery
    {
        IEnumerable<ArticleCategoryListModel> Execute();
    }
}