﻿namespace Tada.Cms.Web.Query.Endpoints.ArticleCategoryList
{
    using System.Collections.Generic;
    using System.Web.Http;

    public class ArticleCategoryListController : ApiController
    {
        private readonly IArticleCategoryListQuery query;

        public ArticleCategoryListController(IArticleCategoryListQuery query)
        {
            this.query = query;
        }

        public IEnumerable<ArticleCategoryListModel> GetArticleCategoryList()
        {
            return this.query.Execute();
        }
    }
}