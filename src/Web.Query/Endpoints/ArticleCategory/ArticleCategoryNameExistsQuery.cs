﻿namespace Tada.Cms.Web.Query.Endpoints.ArticleCategory
{
    using System;
    using System.Data;

    using ServiceStack.OrmLite;

    public class ArticleCategoryNameExistsQuery : IArticleCategoryNameExistsQuery
    {
        private readonly IDbConnection connection;

        public ArticleCategoryNameExistsQuery(IDbConnection connection)
        {
            this.connection = connection;
        }

        public Guid? Execute(Guid? inCategoryId, string name, Guid? exceptCategoryId)
        {
            string sql;
            if (inCategoryId.HasValue)
            {
                sql = "SELECT Id FROM [ArticleCategory] WHERE ParentId = {0} AND Name = {1}";
            }
            else
            {
                sql = "SELECT Id FROM [ArticleCategory] WHERE ParentId IS NULL AND Name = {1}";
            }

            if (exceptCategoryId.HasValue)
            {
                sql += " AND Id <> {2}";
            }

            return this.connection.ScalarFmt<Guid?>(sql, inCategoryId, name, exceptCategoryId);
        }
    }
}