﻿namespace Tada.Cms.Web.Query.Endpoints.ArticleCategory
{
    using System;
    using System.Web.Http;

    public class ArticleCategoryController : ApiController
    {
        private readonly IArticleCategoryQuery query;

        private readonly IArticleCategoryNameExistsQuery existsQuery;

        public ArticleCategoryController(IArticleCategoryQuery query, IArticleCategoryNameExistsQuery existsQuery)
        {
            this.query = query;
            this.existsQuery = existsQuery;
        }

        [HttpGet]
        [ActionName("Get")]
        public ArticleCategoryModel GetArticleCategory(Guid id)
        {
            return this.query.Execute(id);
        }

        [HttpGet]
        [ActionName("exists")]
        public bool Exists(string name, Guid? category = null, Guid? except = null)
        {
            return this.existsQuery.Execute(category, name, except).HasValue;
        }
    }
}