﻿namespace Tada.Cms.Web.Query.Endpoints.ArticleCategory
{
    using System;

    public interface IArticleCategoryQuery
    {
        ArticleCategoryModel Execute(Guid articleCategoryId);
    }
}