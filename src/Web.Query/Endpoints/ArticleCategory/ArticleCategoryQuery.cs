﻿namespace Tada.Cms.Web.Query.Endpoints.ArticleCategory
{
    using System;
    using System.Data;

    using ServiceStack.OrmLite;

    public class ArticleCategoryQuery : IArticleCategoryQuery
    {
        private readonly IDbConnection connection;

        public ArticleCategoryQuery(IDbConnection connection)
        {
            this.connection = connection;
        }

        public ArticleCategoryModel Execute(Guid articleCategoryId)
        {
            return
                this.connection.SingleFmt<ArticleCategoryModel>(
                    "SELECT c.[Id] As ArticleCategoryId, c.[Name], p.[Id] As ParentCategoryId, p.[Name] As ParentCategoryName FROM [ArticleCategory] c LEFT JOIN [ArticleCategory] p ON c.[ParentId] = p.[Id] WHERE c.[Id] = {0}",
                    articleCategoryId);
        }
    }
}