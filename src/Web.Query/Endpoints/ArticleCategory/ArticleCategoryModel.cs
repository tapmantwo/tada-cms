﻿namespace Tada.Cms.Web.Query.Endpoints.ArticleCategory
{
    using System;

    public class ArticleCategoryModel
    {
        public Guid ArticleCategoryId { get; set; }

        public string Name { get; set; }

        public Guid ParentCategoryId { get; set; }

        public string ParentCategoryName { get; set; }
    }
}