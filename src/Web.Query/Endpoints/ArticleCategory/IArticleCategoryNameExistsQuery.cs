﻿namespace Tada.Cms.Web.Query.Endpoints.ArticleCategory
{
    using System;

    public interface IArticleCategoryNameExistsQuery
    {
        Guid? Execute(Guid? inCategoryId, string name, Guid? exceptCategoryId);
    }
}