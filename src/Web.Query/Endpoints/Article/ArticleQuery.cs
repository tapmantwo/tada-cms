﻿namespace Tada.Cms.Web.Query.Endpoints.Article
{
    using System;
    using System.Data;

    using ServiceStack.OrmLite;

    public class ArticleQuery : IArticleQuery
    {
        private readonly IDbConnection connection;

        public ArticleQuery(IDbConnection connection)
        {
            this.connection = connection;
        }

        public ArticleModel Execute(Guid articleId)
        {
            return this.connection.SingleFmt<ArticleModel>("SELECT [Id] AS [ArticleId], [Content], [Summary], [Title] FROM [Article] WHERE [Id] = {0}", articleId);
        }
    }
}