﻿namespace Tada.Cms.Web.Query.Endpoints.Article
{
    using System;
    using System.Web.Http;

    public class ArticleController : ApiController
    {
        private readonly IArticleQuery query;

        public ArticleController(IArticleQuery query)
        {
            this.query = query;
        }

        public ArticleModel GetArticle(Guid id)
        {
            return this.query.Execute(id);
        }
    }
}