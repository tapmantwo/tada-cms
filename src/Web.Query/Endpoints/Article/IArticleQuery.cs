﻿namespace Tada.Cms.Web.Query.Endpoints.Article
{
    using System;

    public interface IArticleQuery
    {
        ArticleModel Execute(Guid articleId);
    }
}