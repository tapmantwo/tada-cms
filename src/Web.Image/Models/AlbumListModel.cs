﻿namespace Tada.Cms.Web.Image.Endpoints.AlbumList
{
    public class AlbumListModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string CoverPhotoUrl { get; set; }
        public long ImageCount { get; set; }
        public string Link { get; set; }
    }
}