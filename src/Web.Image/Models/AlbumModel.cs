﻿using System.Collections.Generic;

namespace Tada.Cms.Web.Image.Endpoints.Album
{
    public class AlbumModel
    {
        public AlbumModel()
        {
            Photos = new List<Photo>();
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public string CoverPhotoUrl { get; set; }
        public List<Photo> Photos { get; set; }
        public long ImageCount { get; set; }
    }

    public class Photo
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string ThumbUrl { get; set; }
        public long Width { get; set; }
        public long Height { get; set; }
    }
}