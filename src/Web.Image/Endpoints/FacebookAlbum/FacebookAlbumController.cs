﻿using System.Web.Http;
using Tada.Cms.Web.Image.Endpoints.Album;
using Tada.Cms.Web.Image.Facebook;

namespace Tada.Cms.Web.Image.Endpoints.FacebookAlbum
{
    public class FacebookAlbumController : ApiController
    {
        [HttpGet]
        [ActionName("get")]
        public AlbumModel GetAlbum(string id)
        {
            var batchResults = FacebookHelper.ExecuteBatch(new[]
                {
                    id,
                    string.Format("{0}/photos?limit=1000",id)
                });

            if (batchResults == null)
                return null;

            var rawAlbum = batchResults[0];
            var rawPhotos = batchResults[1].data;

            var album = new AlbumModel
            {
                Id = rawAlbum.id,
                Name = rawAlbum.name,
                CoverPhotoUrl = string.Format("{0}{1}/picture?type=normal", FacebookHelper.FacebookBaseUri, rawAlbum.cover_photo),
                ImageCount = rawAlbum.count
            };
            foreach (var rawPhoto in rawPhotos)
            {
                album.Photos.Add(new Photo
                    {
                        Id = rawPhoto.id,
                        Name = rawPhoto.name ?? string.Empty,
                        Url = rawPhoto.source,
                        ThumbUrl = string.Format("{0}{1}/picture?type=album", FacebookHelper.FacebookBaseUri, rawPhoto.id),
                        Width = rawPhoto.width,
                        Height = rawPhoto.height
                    });
            }

            return album;
        }
    }
}