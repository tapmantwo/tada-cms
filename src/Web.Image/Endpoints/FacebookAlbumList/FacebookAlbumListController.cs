﻿using System.Collections.Generic;
using System.Configuration;
using System.Web.Http;
using Tada.Cms.Web.Image.Endpoints.AlbumList;
using Tada.Cms.Web.Image.Facebook;

namespace Tada.Cms.Web.Image.Endpoints.FacebookAlbumList
{
    public class FacebookAlbumListController : ApiController
    {

        [HttpGet]
        [ActionName("get")]
        public AlbumListModel[] GetAlbumList()
        {
            return GetAlbumList(FacebookHelper.ClientUserId);
        }

        [HttpGet]
        [ActionName("get")]
        public AlbumListModel[] GetAlbumList(string id)
        {
            var facebookUserId = id;
            if (string.IsNullOrEmpty(facebookUserId))
            {
                facebookUserId = FacebookHelper.ClientUserId;
            }
            var albums = new List<AlbumListModel>();

            var result = FacebookHelper.ExecuteGet(string.Format("{0}/albums?limit=1000", facebookUserId));
            if (result == null)
                return null;

            foreach (var rawAlbum in result.data)
            {
                var album = new AlbumListModel
                    {
                        Id = rawAlbum.id,
                        Name = rawAlbum.name ?? string.Empty,
                        CoverPhotoUrl = string.Format("{0}{1}/picture?type=album", FacebookHelper.FacebookBaseUri, rawAlbum.cover_photo ?? string.Empty),
                        ImageCount = rawAlbum.count ?? 0,
                        Link = ConfigurationManager.AppSettings["imageHost"] + "/api/FacebookAlbum/" + rawAlbum.id
                    };
                albums.Add(album);
            }

            return albums.ToArray();
        }
    }
}