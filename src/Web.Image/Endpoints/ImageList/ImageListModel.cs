﻿namespace Tada.Cms.Web.Image.Endpoints.ImageList
{
    /// <summary>
    /// The property names are named to match the requirements for the ckeditor image browser pluging :(
    /// </summary>
    public class ImageListModel
    {
        public string image { get; set; }
        public string thumb { get; set; }
        public string folder { get; set; }
    }
}