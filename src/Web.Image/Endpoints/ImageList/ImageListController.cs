﻿using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Web.Http;
using System.Linq;

namespace Tada.Cms.Web.Image.Endpoints.ImageList
{
    public class ImageListController : ApiController
    {
        private static string _imageHost;
        private static string _rootDir;

        public ImageListController()
        {
            _imageHost = ConfigurationManager.AppSettings["imageHost"];
            _rootDir = ConfigurationManager.AppSettings["imagePath"];
        }

        [HttpGet]
        [ActionName("get")]
        public ImageListModel[] GetImageList()
        {
            var dir = new DirectoryInfo(_rootDir);
            var imageLists = RecurseDirectory(_rootDir, new[] { dir }, new List<ImageListModel>());
            return imageLists.ToArray();
        }

        private static List<ImageListModel> RecurseDirectory(string rootDir, IEnumerable<DirectoryInfo> dirs, List<ImageListModel> imageLists)
        {
            foreach (var directoryInfo in dirs)
            {
                var files = directoryInfo.EnumerateFiles()
                                         .Where(f => f.Extension == ".jpg" && !f.Name.EndsWith("_thumb.jpg"));

                imageLists.AddRange(files.Select(f => Convert(rootDir, f)));
                imageLists = RecurseDirectory(rootDir, directoryInfo.EnumerateDirectories(), imageLists);
            }

            return imageLists;
        }

        private static ImageListModel Convert(string rootDir, FileSystemInfo fileInfo)
        {
            var thumbPath = fileInfo.FullName.Replace(fileInfo.Extension, "_thumb" + fileInfo.Extension);
            var thumbUrl = string.Empty;
            if (File.Exists(thumbPath))
                thumbUrl = thumbPath.Replace(rootDir, _imageHost).Replace("\\", "/");

            return new ImageListModel
                {
                    image = fileInfo.FullName.Replace(rootDir, _imageHost).Replace("\\", "/"),
                    thumb = thumbUrl,
                    folder = fileInfo.FullName.Replace(rootDir, "root").Replace(fileInfo.Name, "").TrimEnd('\\')
                };
        }
    }
}