﻿using System;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Linq;

namespace Tada.Cms.Web.Image.Endpoints.Image
{
    public class ImageController : ApiController
    {
        private static string _imageHost;
        private static string _rootDir;

        public ImageController()
        {
            _imageHost = ConfigurationManager.AppSettings["imageHost"];
            _rootDir = ConfigurationManager.AppSettings["imagePath"];
        }

        [HttpPost]
        public Task<HttpResponseMessage> Upload(string ckEditorFuncNum, string ckEditor, string langCode,
                                                string targetPath)
        {
            Task<HttpResponseMessage> taskResponse;
            var request = Request;
            if (!request.Content.IsMimeMultipartContent())
            {
                var script = GetResponseScript(ckEditorFuncNum, string.Empty);
                var responseMessage = new HttpResponseMessage()
                    {
                        Content = new StringContent(script, Encoding.UTF8)
                    };

                responseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");

                taskResponse = Task<HttpResponseMessage>.Factory.StartNew(() => responseMessage);
            }
            else
            {
                var provider = new MultipartMemoryStreamProvider();

                taskResponse = request.Content.ReadAsMultipartAsync(provider).
                                       ContinueWith(o =>
                                           {
                                               string script;
                                               if (!provider.Contents.Any())
                                               {
                                                   script = GetResponseScript(ckEditorFuncNum, string.Empty);
                                               }
                                               else
                                               {
                                                   var file = provider.Contents[0];
                                                   var filename = file.Headers.ContentDisposition.FileName.Trim('\"');
                                                   var buffer = file.ReadAsByteArrayAsync();
                                                   var url = SaveFileAndGetUrl(filename, targetPath, buffer);
                                                   script = GetResponseScript(ckEditorFuncNum, url);
                                               }

                                               var responseMessage = new HttpResponseMessage()
                                                   {
                                                       Content = new StringContent(script, Encoding.UTF8)
                                                   };

                                               responseMessage.Content.Headers.ContentType =
                                                   new MediaTypeHeaderValue("text/html");
                                               return responseMessage;
                                           }
                    );
            }
            return taskResponse;
        }

        private static string SaveFileAndGetUrl(string filename, string subFolder, Task<byte[]> buffer)
        {
            var fileInfo = new FileInfo(filename);

            var fileBytes = buffer.Result;
            var thumbFileBytes = ResizeBytes(fileBytes);

            var middlePath = string.Format("\\{0}", subFolder);

            var targetPath = string.Format("{0}{1}", _rootDir, middlePath);
            if (!Directory.Exists(targetPath))
                Directory.CreateDirectory(targetPath);

            var targetPathAndFile = string.Format("{0}\\{1}", targetPath, filename);

            var targetPathAndFileThumb = string.Format("{0}\\{1}", targetPath,
                                                       fileInfo.Name.Replace(fileInfo.Extension,
                                                                             "_thumb" + fileInfo.Extension));

            if (File.Exists(targetPathAndFile))
                return string.Format("{0}{1}/{2}", _imageHost, middlePath, filename).Replace('\\', '/');

            if (File.Exists(targetPathAndFileThumb))
                File.Delete(targetPathAndFileThumb);

            File.WriteAllBytes(targetPathAndFile, fileBytes);
            File.WriteAllBytes(targetPathAndFileThumb, thumbFileBytes);

            return string.Format("{0}{1}/{2}", _imageHost, middlePath, filename).Replace('\\', '/');
        }

        private static string GetResponseScript(string ckEditorFuncNum, string url)
        {
            var errorMessage = string.Empty;

            if (url == string.Empty)
                errorMessage = ", 'failed to save image. An file by the same name may already exist.'";

            const string format = "<html><body><script>window.parent.CKEDITOR.tools.callFunction({0}, '{1}'{2});</script></body></html>";

            var output = string.Format(format, ckEditorFuncNum, url, errorMessage);
            return output;
        }

        private static byte[] ResizeBytes(byte[] imageBytes)
        {
            var sourceStream = new MemoryStream(imageBytes);
            var source = System.Drawing.Image.FromStream(sourceStream, true);
            const int newWidth = 100;
            var newHeight = ((source.Height / (decimal)source.Width) * newWidth);
            var scaled = ResizeImage(source, newWidth, (int)newHeight);

            source.Dispose();

            var stream = new MemoryStream();

            scaled.Save(stream, ImageFormat.Jpeg);
            return stream.ToArray();
        }

        /// <summary>
        /// Resize the image to the specified width and height.
        /// </summary>
        /// <param name="image">The image to resize.</param>
        /// <param name="width">The width to resize to.</param>
        /// <param name="height">The height to resize to.</param>
        /// <returns>The resized image.</returns>
        private static Bitmap ResizeImage(System.Drawing.Image image, int width, int height)
        {
            var result = new Bitmap(width, height);

            // Copy across the EXIF data
            foreach (var property in image.PropertyItems)
            {
                result.SetPropertyItem(property);
            }

            // set the resolutions the same to avoid cropping due to resolution differences
            result.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            //use a graphics object to draw the resized image into the bitmap
            using (Graphics graphics = Graphics.FromImage(result))
            {
                //set the resize quality modes to high quality
                graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

                //draw the image into the target bitmap
                graphics.DrawImage(image, 0, 0, result.Width, result.Height);
            }

            //return the resulting bitmap
            return result;
        }






    }
}