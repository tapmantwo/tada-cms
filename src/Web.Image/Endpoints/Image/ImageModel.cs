﻿namespace Tada.Cms.Web.Image.Endpoints.Image
{
    public class ImageModel
    {
        public string Image { get; set; }
        public string Thumb { get; set; }
        public string Folder { get; set; }
    }
}