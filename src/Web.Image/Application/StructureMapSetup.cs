﻿namespace Tada.Cms.Web.Image.Application
{
    using System.Configuration;
    using System.Web.Http;

    using StructureMap;

    using Tada.Cms.Core.Application;

    public static class StructureMapSetup
    {
        public static void Configure()
        {
            ObjectFactory.Configure(
                cfg => cfg.Scan(
                    scanner =>
                        {
                            scanner.AssemblyContainingType<System.Web.Http.Hosting.IHostBufferPolicySelector>();
                            scanner.Assembly("Tada.Cms.Web.Image");
                            scanner.WithDefaultConventions();
                        }));
        }
    }
}