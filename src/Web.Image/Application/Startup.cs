﻿using System.Configuration;
using System.Net.Http.Headers;
using Microsoft.Owin.FileSystems;
using Microsoft.Owin.StaticFiles;

namespace Tada.Cms.Web.Image.Application
{
    using System.Threading.Tasks;
    using System.Web.Cors;
    using System.Web.Http;

    using Microsoft.Owin.Cors;
    using Microsoft.Owin.Diagnostics;

    using Owin;

    using StructureMap;

    public class Startup
    {
        // This code configures Web API. The Startup class is specified as a type
        // parameter in the WebApp.Start method.
        public void Configuration(IAppBuilder appBuilder)
        {
            // Configure Web API for self-host. 
            var config = new HttpConfiguration();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional });
            config.Routes.IgnoreRoute("api", "!api/");

            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));

            StructureMapSetup.Configure();

            config.DependencyResolver = new StructureMapDependencyResolver();

            appBuilder.UseCors(new CorsOptions
            {
                PolicyProvider = new CorsPolicyProvider
                {
                    PolicyResolver = context => Task.FromResult(new CorsPolicy
                    {
                        AllowAnyHeader = true,
                        AllowAnyMethod = true,
                        AllowAnyOrigin = true,
                        SupportsCredentials = false
                    })
                }
            });

            appBuilder.UseErrorPage(
                new ErrorPageOptions
                {
                    ShowExceptionDetails = true,
                    ShowEnvironment = true,
                    ShowQuery = true,
                    ShowHeaders = true,
                    ShowSourceCode = true,
                    SourceCodeLineCount = 100
                });

            appBuilder.UseWebApi(config);

            var fileSystem = new PhysicalFileSystem(ConfigurationManager.AppSettings["imagePath"]);

            var options = new FileServerOptions
            {
                EnableDirectoryBrowsing = true,
                FileSystem = fileSystem
            };

            appBuilder.UseFileServer(options);
        }
    } 
}
