﻿using System;
using System.Configuration;
using System.Linq;
using Facebook;

namespace Tada.Cms.Web.Image.Facebook
{
    public static class FacebookHelper
    {
        public static string AppId
        {
            get { return ConfigurationManager.AppSettings["appId"]; }
        }

        public static string AppSecret
        {
            get { return ConfigurationManager.AppSettings["appSecret"]; }
        }

        public static string ClientUserId
        {
            get { return ConfigurationManager.AppSettings["clientFacebookUserId"]; }
        }

        public static dynamic ExecuteGet(string requestPath)
        {
            try
            {
                var client = new FacebookClient(GetAuthenticationToken());
                return client.Get(requestPath);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static dynamic ExecuteBatch(string[] requestPaths)
        {
            try
            {
                var client = new FacebookClient(GetAuthenticationToken());
                var batches = requestPaths.Select(requestPath => new FacebookBatchParameter(requestPath)).ToList();
                return client.Batch(batches.ToArray());
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static string GetAuthenticationToken()
        {
            var fb = new FacebookClient();
            var authPath = string.Format(
                "oauth/access_token?client_id={0}&client_secret={1}&grant_type=client_credentials",
                AppId,
                AppSecret
                );
            dynamic result = fb.Get(authPath);
            return result.access_token;
        }

        public static string FacebookBaseUri = "http://graph.facebook.com/";
    }
}