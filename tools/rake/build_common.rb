include FileTest

# Derived settings
TASKS = Rake.application.top_level_tasks
CONFIG_TARGET = (!ENV["config"] && (TASKS.include?('spec') || TASKS.include?('full') || TASKS.include?('develop') || TASKS.include?('e2edevelop'))) ? 'debug' : CONFIG.downcase

# Paths
BASE_PATH = "#{File.expand_path(File.dirname(__FILE__))}/../.."
SOURCE_PATH = "#{BASE_PATH}/src"
TESTS_PATH = "#{BASE_PATH}/src"
SPECS_PATH = "#{BASE_PATH}/specs"
BUILD_PATH = "#{BASE_PATH}/build"
RESULTS_PATH = "#{BASE_PATH}/results"
ARTIFACTS_PATH = "#{BASE_PATH}/artifacts"
NUSPEC_PATH = "#{BASE_PATH}/packaging/nuspec"
NUGET_PATH = "#{ARTIFACTS_PATH}/nuget"
TOOLS_PATH = "#{BASE_PATH}/tools"
DATA_PATH = ENV["DATA_PATH"] || "#{BASE_PATH}/data"
CONFIG_PATH = "#{BASE_PATH}/config"
NPM_PATH = "#{TOOLS_PATH}/node_modules"
WEB_PATH = "#{SOURCE_PATH}/Web"
WEB_JS_PATH = "#{WEB_PATH}/app"
WEB_JS_VERSION = "#{WEB_JS_PATH}/global/01-application/00-version.js"
WEB_CSS_PATH = "#{WEB_PATH}/styling"
BUILD_TARGET_PATH = !ENV["OUT_DIR"].nil? ? ENV["OUT_DIR"].gsub('\\','/') : (defined?(WEB_PLATFORM) ? "#{BUILD_PATH}/#{WEB_PLATFORM.downcase}/#{CONFIG_TARGET}" : "#{BUILD_PATH}/#{PLATFORM.downcase}/#{CONFIG_TARGET}")
BUILD_DIAGNOSTICS_PATH = "#{BUILD_TARGET_PATH}/web/diagnostics"
BUILD_SOURCEMAPS_PATH = "#{BUILD_DIAGNOSTICS_PATH}/sourcemaps"
BUILD_SOURCEDIAG_PATH = "#{BUILD_DIAGNOSTICS_PATH}/source"
BUILD_WEB_PATH = "#{BUILD_TARGET_PATH}/web"
BUILD_JS_PATH = "#{BUILD_WEB_PATH}/app"

# Build configuration
PREBUILD_NUMBER = !ENV["PREBUILD_NUMBER"].nil? ? ENV["PREBUILD_NUMBER"] : nil
BUILD_NUMBER = !PREBUILD_NUMBER.nil? ? PREBUILD_NUMBER : "#{BUILD_VERSION}.#{(ENV["BUILD_NUMBER"] || Time.new.strftime('5%H%M'))}"
TEAMCITY = (!ENV["BUILD_NUMBER"].nil? or !ENV["TEAMCITY_BUILD_PROPERTIES_FILE"].nil?)
TEAMCITY_BRANCH = !TEAMCITY ? nil : ENV["BRANCH"]
TEAMCITY_FORK = ENV["FORK"] || nil
WEB_ENABLED = Dir.exists?(WEB_JS_PATH)
RUN_COMPASS = WEB_ENABLED && Dir.exists?("#{WEB_PATH}/sass")
DATA_ENABLED = Dir.exists?(DATA_PATH) and !TEAMCITY
DATA_FORCEBUILD = ENV["PURGE"] || true
DATA_CONFIG = ENV["DATA"] || "master"
DATA_SERVICE_NAME = ENV["servicename"] || nil

# Files
ASSEMBLY_INFO = "#{SOURCE_PATH}/CommonAssemblyInfo.cs"
SOLUTION_PATHNAME = "#{SOURCE_PATH}/#{SOLUTION_FILE}"
VERSION_INFO = "#{BASE_PATH}/VERSION.txt"

# Matching
TEST_ASSEMBLY_PATTERN_PREFIX = "Tests"
TEST_ASSEMBLY_PATTERN_UNIT = "#{TEST_ASSEMBLY_PATTERN_PREFIX}.Unit"
TEST_ASSEMBLY_PATTERN_INTEGRATION = "#{TEST_ASSEMBLY_PATTERN_PREFIX}.Integration"
TEST_ASSEMBLY_PATTERN_SPEC = "#{TEST_ASSEMBLY_PATTERN_PREFIX}.Acceptance"
TEST_ASSEMBLY_PATTERN_SYSTEM = "#{TEST_ASSEMBLY_PATTERN_PREFIX}.System"
TEST_ASSEMBLY_PATTERN_REGRESSION = "#{TEST_ASSEMBLY_PATTERN_PREFIX}.Regression"

# Commands
XUNIT_COMMAND = "#{TOOLS_PATH}/xunit/xunit.console.clr4.#{(PLATFORM.empty? or PLATFORM.eql?('x86') ? 'x86.' : '')}exe"
NUGET_COMMAND = "#{SOURCE_PATH}/.nuget/NuGet.exe"
KARMA_COMMAND = "#{NPM_PATH}/.bin/karma"
KARMA_CONFIG_UNIT = nil unless defined?(KARMA_CONFIG_UNIT)
KARMA_CONFIG_INT = nil unless defined?(KARMA_CONFIG_INT)
KARMA_CONFIG_SPEC = nil unless defined?(KARMA_CONFIG_SPEC)
KARMA_CONFIG_E2E = nil unless defined?(KARMA_CONFIG_E2E)
KARMA_CONFIG_UNIT_PATHNAMES = KARMA_CONFIG_UNIT.nil? ? nil : FileList["#{CONFIG_PATH}/#{KARMA_CONFIG_UNIT}"]
KARMA_CONFIG_INT_PATHNAMES = KARMA_CONFIG_INT.nil? ? nil : FileList["#{CONFIG_PATH}/#{KARMA_CONFIG_INT}"]
KARMA_CONFIG_SPEC_PATHNAMES = KARMA_CONFIG_SPEC.nil? ? nil : FileList["#{CONFIG_PATH}/#{KARMA_CONFIG_SPEC}"]
KARMA_CONFIG_E2E_PATHNAMES = KARMA_CONFIG_E2E.nil? ? nil : FileList["#{CONFIG_PATH}/#{KARMA_CONFIG_E2E}"]
UGLIFY_COMMAND = "#{NPM_PATH}/.bin/uglifyjs"
JSHINT_COMMAND = "#{NPM_PATH}/.bin/jsHint"
COMPASS_COMMAND = "compass"

# Command spawn
SPAWN_COMMAND = !ENV["SPAWN"].nil? ? ENV["SPAWN"] : "powershell -Command \"Start-Process [COMMANDARGS] -WorkingDirectory [DIR] -FilePath [COMMAND] -ArgumentList [ARGS]\""
BUS_COMMAND = "NServiceBus.Host.exe"

# Global vars
error_count = 0

# Set up our build system
require 'albacore'
require 'pathname'
require 'rake/clean'
require 'rexml/document'
require 'nokogiri' unless !WEB_ENABLED

# Check dependencies
raise "You do not have the required dependencies, run '.\\InstallGem.bat' or './installgem.sh'." \
    unless `bundle check`.include? "The Gemfile's dependencies are satisfied\n"

# Configure albacore
Albacore.configure do |config|
    config.log_level = (TEAMCITY ? :verbose : :normal)

    config.msbuild.solution = SOLUTION_PATHNAME
    config.msbuild.properties = { :configuration => CONFIG_TARGET, :platform => PLATFORM }
    config.msbuild.use :net4
    config.msbuild.targets = [ :Clean, :Build ]
    config.msbuild.verbosity = "normal"

    CLEAN.include(FileList["#{SOURCE_PATH}/**/obj"])
    CLEAN.include(FileList["#{WEB_JS_PATH}/**/*.min.js"]) if WEB_ENABLED
    CLEAN.include(FileList["#{WEB_CSS_PATH}/*.css"]) if RUN_COMPASS

    CLOBBER.include(NUGET_PATH)
    CLOBBER.include(FileList["#{SOURCE_PATH}/**/bin"])
    CLOBBER.include(ARTIFACTS_PATH)
    CLOBBER.include(BUILD_PATH)
    CLOBBER.include(RESULTS_PATH)
    CLOBBER.include(BUILD_WEB_PATH) if RUN_COMPASS
end

# Tasks
task :default => [:spec]

desc "Build"
task :build => [:init, :assemblyinfo, :packagerestore] do
    Rake::Task[:msbuild].invoke
end

task :postbuild => [:jshint, :uglify, :compass_compile] do    
end

task :packagerestore do
    sh "#{NUGET_COMMAND} restore #{SOLUTION_PATHNAME}"
end

desc "Build + Unit & Integration Tests (default)"
task :test => [:build] do
    Rake::Task[:jsunit].invoke

    error_count = (error_count || 0) + RunTests("#{TEST_ASSEMBLY_PATTERN_UNIT}")
    error_count = (error_count || 0) + RunTests("#{TEST_ASSEMBLY_PATTERN_INTEGRATION}")

    raise "\nTest errors: #{error_count}\n" unless error_count == 0 || !TASKS.include?('test')
end

desc "Build + Unit tests"
task :quick => [:build] do
    Rake::Task[:jsunit].invoke
    
    error_count = (error_count || 0) + RunTests(TEST_ASSEMBLY_PATTERN_UNIT)

    raise "\nTest errors: #{error_count}\n" unless error_count == 0 || !TASKS.include?('quick')
end

desc "Acceptance tests (specs)"
task :spec => [:build, :jsunit] do
    if WEB_ENABLED
        RunKarma KARMA_CONFIG_SPEC_PATHNAMES, true, 'Karma spec', TASKS.include?('full')
    end
end

desc "Run development mode against test data"
task :develop => [:build] do
    if WEB_ENABLED  
        RunKarma(KARMA_CONFIG_SPEC_PATHNAMES, false, 'Karma spec')
        RunKarma(KARMA_CONFIG_UNIT_PATHNAMES, false, 'Karma unit')
        
        if RUN_COMPASS
            SpawnCommand(COMPASS_COMMAND, "watch #{WEB_PATH}")
        end
    end
end

desc "Build data (local)"
task :data do
    is_self = BASE_PATH.gsub(/\\/, "/").downcase.start_with?(DATA_PATH.gsub(/\\/, "/").downcase)

    if !is_self and DATA_ENABLED and (DATA_FORCEBUILD or !Dir.exists?("#{DATA_PATH}/build"))
        Dir.chdir(DATA_PATH)
        
        branch_change = `git checkout #{DATA_CONFIG}`
        raise "Unable to perform git branch checkout on data to #{DATA_CONFIG}." if branch_change.include?("error:")

        Dir.chdir(BASE_PATH)

        SpawnCommand("rake", "run servicename=#{SERVICE_NAME} prebuild_number=#{BUILD_NUMBER}", DATA_PATH, "[Data Build]", "-Wait")
    end
end

desc "Build + Run"
task :run => [:build, :data] do
    RunBuilds BUILD_PATH
end

desc "Build + Tests + Specs"
task :full => [:test, :spec ] do
    error_count = (error_count || 0) + RunTests("#{TEST_ASSEMBLY_PATTERN_SYSTEM}")
    error_count = (error_count || 0) + RunTests("#{TEST_ASSEMBLY_PATTERN_REGRESSION}")

    raise "\nTest errors: #{error_count}\n" unless error_count == 0 || !TASKS.include?('full')
end

desc "Reset build metadata"
task :reset do
    indexupdate = `git checkout #{SOURCE_PATH}/CommonAssemblyInfo.cs`
    raise "Unable to perform git checkout operation, cannot continue (#{indexupdate})." unless indexupdate.empty?

    if WEB_ENABLED
        indexupdate = `git checkout #{WEB_JS_VERSION}`
        raise "Unable to perform git checkout operation, cannot continue (#{indexupdate})." unless indexupdate.empty?
    end
end

# Hidden tasks
task :setup do
    if not TEAMCITY
        indexupdate = `git update-index --assume-unchanged #{SOURCE_PATH}/CommonAssemblyInfo.cs`
        raise "Unable to perform git index operation, cannot continue (#{indexupdate})." unless indexupdate.empty?

        if WEB_ENABLED
            indexupdate = `git update-index --assume-unchanged #{WEB_JS_VERSION}`
            raise "Unable to perform git index operation, cannot continue (#{indexupdate})." unless indexupdate.empty?
        end

        if Dir.exists?("#{SOURCE_PATH}/.data/")            
            FileList.new("#{SOURCE_PATH}/.data/*").collect! {|e|
                indexupdate = `git update-index --assume-unchanged #{e}`
                raise "Unable to perform git index operation, cannot continue (#{indexupdate})." unless indexupdate.empty?
            }
        end
    end

    Dir.mkdir BUILD_PATH unless File.exists?(BUILD_PATH)
    Dir.mkdir RESULTS_PATH unless File.exists?(RESULTS_PATH)
    Dir.mkdir ARTIFACTS_PATH unless File.exists?(ARTIFACTS_PATH)
    Dir.mkdir NUGET_PATH unless File.exists?(NUGET_PATH)

    if WEB_ENABLED
        CheckDirExists BUILD_DIAGNOSTICS_PATH
        CheckDirExists BUILD_SOURCEMAPS_PATH
        CheckDirExists BUILD_SOURCEDIAG_PATH
        CheckDirExists BUILD_WEB_PATH
    end
end

task :env do
    ENV["PHANTOMJS_BIN"] = "#{NPM_PATH}/karma-phantomjs-launcher/node_modules/phantomjs/lib/phantom/phantomjs"
    
    if ENV["CHROME_BIN"].nil? or ENV["CHROME_BIN"].empty?
        ENV["CHROME_BIN"] = "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe"
    end

    if ENV["FIREFOX_BIN"].nil? or ENV["FIREFOX_BIN"].empty?
        ENV["FIREFOX_BIN"] = "C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe"
    end
end

task :init => [:clobber, :env, :setup] do
end

task :jsunit => [:env, :setup] do
    RunKarma KARMA_CONFIG_UNIT_PATHNAMES, true, 'Karma unit', true
end

msbuild :msbuild

task :assemblyinfo do
    @asm = AssemblyInfoCustom.new
    SetupAssemblyInfo(@asm)
    @asm.execute
end

task :compass_compile do
    puts "Compiling SCSS files"
    sh "#{COMPASS_COMMAND} compile #{WEB_PATH}"
    FileUtils.cp_r(WEB_CSS_PATH, BUILD_WEB_PATH)
end

task :jshint do
    allJSFiles = FileList.new("#{WEB_JS_PATH}/**/*.js")
    if allJSFiles.length > 0
        puts "Running JSHint"
        commandLineFiles = allJSFiles.join(" ")
        errors = 0;
        allJSFiles.each do |jsFile|
            hintOutput = `#{NPM_PATH}/.bin/jshint --reporter=jslint #{jsFile}`
            if TEAMCITY
                File.open( 'jshint.xml', 'w') do |f|
                    f.write(hintOutput)
                end
            else
                doc = Nokogiri::XML(hintOutput)
                files = doc.xpath('//file')

                files.each do |file|
                    puts "\n  #{file.attr('name')}"

                    file.xpath('issue').each do |issue|
                        puts "\n", "  [#{issue.attr('line')}:#{issue.attr('char')}] #{issue.attr('reason')}"
                        puts "   -> #{issue.attr('evidence')}","\n"
                    end
                end
                (errors = errors + 1) unless files.length == 0
            end
        end

        raise "Exiting due to JSHint errors." unless errors == 0
    end
end

task :uglify => [:setup] do
    if File.exists?(WEB_JS_PATH)
        Dir.chdir(WEB_PATH) do
            RunUglify(WEB_JS_PATH)
        end

        FileList["#{WEB_JS_PATH}/**/*.cshtml", "#{WEB_JS_PATH}/**/*.html"].each { |f|
            ParseHtmlJs f
        }
    end
end

desc "Recreate and redeploy css and (minified) javascript files"
task :refresh => [:uglify, :compass_compile] do    
end

# Helper methods
def ParseHtmlJs(file)
    open(file, 'r') do |f|
        doc = Nokogiri::HTML(f)
        
        tags = doc.xpath("//*[@src != '' or (@href != '' and not(starts-with(@href, '#')))]")
        elements = doc.xpath("//*[(not(starts-with(@src, 'http')) and @src != '') or (starts-with(@href, '../../'))]")

        raise "\nUnable to find script tags in '#{File.basename(f)}', possible invalid document format.\n" if elements.length == 0 and tags.length > 0

        output_file = file.gsub(WEB_JS_PATH, BUILD_JS_PATH)
        
        CheckDirExists(File.dirname(output_file))

        if elements.length == 0 and tags.length == 0
            cp file, output_file
        else
            current_prefix = nil

            elements.sort_by{|e| e.attr('src') || ''}.each { |e|
                if (e.name.eql?('script'))
                    path = e.attr('src').to_s()

                    if path.match(/^..\/..\//i)
                        e.attributes['src'].value = path[3..-1]
                    else
                        prefix = !path.match(/^..\//i) ? path[0, path.index('/')] : path[path.rindex('../') + 3, path.index('/', path.rindex('../') + 3) - 3]

                        if (!current_prefix.eql?(prefix))
                            current_prefix = prefix
                            e.attributes['src'].value = "#{path[0, path.index("#{prefix}/")]}#{prefix}/#{prefix}.min.js"
                        else
                            e.remove
                        end
                    end
                else
                    attr_name = e.attributes['href'].nil? ? 'src' : 'href'

                    e.attributes[attr_name].value = e.attr(attr_name).to_s()[3..-1] if e.attributes[attr_name].value.match(/^..\/..\//i)
                end
            }

            open(output_file, 'w') do |w|
                w.write(doc.to_xml(:save_with => Nokogiri::XML::Node::SaveOptions::AS_HTML | Nokogiri::XML::Node::SaveOptions::FORMAT).gsub(/[\r\n]+/, "\n"))
            end
        end

        project_root = file.gsub(WEB_PATH, "")
        project_file = file.gsub(SOURCE_PATH, "")[1, file.index('/', SOURCE_PATH.length + 1) - (SOURCE_PATH.length + 1)]

        Dir.glob("#{SOURCE_PATH}/#{project_file}.Tests.*/").each { |f| 
            test_file = "#{f}bin/#{CONFIG_TARGET}#{project_root}"
            CheckDirExists File.dirname(test_file)
            FileUtils.cp(output_file, "#{f}bin/#{CONFIG_TARGET}#{project_root}") if File.directory?(f)
        }
    end
end

def write_at(fname, at_line, sdat)
  open(fname, 'r+') do |f|
    while (at_line-=1) > 0          # read up to the line you want to write after
      f.readline
    end
    pos = f.pos                     # save your position in the file
    rest = f.read                   # save the rest of the file
    f.seek pos                      # go back to the old position
    f.write sdat                    # write the inserted value
    f.write rest                    # write the remainder of the file
  end
end

def RunBuilds(path, args = '', pattern = nil)
    conEmu = SPAWN_COMMAND.include?('-new-console')

    if !path.nil?
        directories = Dir.glob("#{path}/*").select { |f| File.directory? f }

        directories.each { |d| d 
            dirname = File.basename(d)

            if (d.to_s().downcase.include?(CONFIG_TARGET) and (pattern.nil? or d.to_s().match(pattern)))
                if dirname.match(/web$/i) or dirname.match(/web.command$/i) or dirname.match(/web.query$/i) or dirname.match(/.http$/i)
                    FileList.new("#{path}/#{dirname}/**")
                            .collect! { |e|
                                if e.match(/web.exe$/i) or e.match(/web.query.exe$/i) or e.match(/web.command.exe$/i) or e.match(/http.exe$/i)
                                    SpawnCommand(e, args, "#{path}/#{dirname}/", "[WEB]-#{SOLUTION_NAME}")
                                end
                            }
                elsif dirname.match(/diagnostics$/i)
                    FileList.new("#{path}/#{dirname}/**")
                            .collect! { |e|
                                if e.match(/diagnostics.exe$/i)
                                    SpawnCommand(e, args, "#{path}/#{dirname}/", "[DIAG]-#{SOLUTION_NAME}")
                                end
                            }
                elsif dirname.match(/installer$/i)
                    FileList.new("#{path}/#{dirname}/**")
                            .collect! { |e|
                                if e.match(/installer.exe$/i)
                                    SpawnCommand(e, "/embedded /assembly \\\"#{BUILD_TARGET_PATH}/Deploy/AHC.Data.Apex.#{DATA_SERVICE_NAME}.Deploy.Dll\\\" /version \\\"#{BUILD_NUMBER}\\\" /connectionstring \\\"Data Source=#{BUILD_PATH}/#{DATA_SERVICE_NAME}.vdb5\\\" /nosafe" + args, "#{path}/#{dirname}/", "[DATA]-#{SOLUTION_NAME}", "-Wait")
                                end
                            }
                elsif dirname.match(/^bus./i) and !dirname.match(/.messages$/i)
                    bus_exe = "#{path}/#{dirname}/#{BUS_COMMAND}"
                    bus_args = ""

                    FileList.new("#{path}/#{dirname}/**")
                            .collect! { |e|
                                if e.match(/#{dirname}.dll$/i)
                                    FileUtils.cp FileList.new("#{SOURCE_PATH}/.data/*"), "#{path}/#{dirname}/", :verbose => true

                                    SpawnCommand("#{path}/#{dirname}/#{BUS_COMMAND}", " /install #{e}" + args + (conEmu ? " && " + bus_exe + bus_args : ""), "#{path}/#{dirname}/", "[BUS" + (conEmu ? "" : "-INSTALL") + "]-#{SOLUTION_NAME}", "-Wait")
                                end
                            }

                    SpawnCommand(bus_exe, bus_args, "#{path}/#{dirname}/", "[BUS]-#{SOLUTION_NAME}") unless conEmu
                else
                    RunBuilds "#{path}/#{dirname}", args, pattern
                end
            else
                RunBuilds "#{path}/#{dirname}", args, pattern
            end
        }
    end
end

def SpawnCommand(command = nil, args = '', workingdir = "#{BASE_PATH}/", title = nil, commandargs = '')
    if SPAWN_COMMAND.nil? 
        raise "Command spawning is not supported."
    end

    if command.nil?
        raise "Spawn command cannot be nil."
    end

    conEmu = SPAWN_COMMAND.include?('-new-console')

    spawnCommand = SPAWN_COMMAND
    spawnCommand = spawnCommand.gsub("[DIR]", conEmu ? "\"#{workingdir.gsub('/','\\')}\"" : "'#{workingdir}'")
    spawnCommand = spawnCommand.gsub("[COMMAND]", conEmu ? "\"#{command.gsub('/','\\')}\"" : "'#{command}'")
    spawnCommand = spawnCommand.gsub("[ARGS]", args.empty? ? "" : (conEmu ? "#{args}" : "'#{args}'"))
    spawnCommand = spawnCommand.gsub("[COMMANDARGS]", args.empty? ? "" : "#{commandargs} ")
    spawnCommand = spawnCommand.gsub("-ArgumentList ", '') #Hack for powershell
    spawnCommand = spawnCommand.gsub("-new-console", "-new_console" + (title.nil? ? "" : ":n:t:#{title.gsub(' ', '_')}")) #Hack for ConEmu
    
    puts " -> #{spawnCommand}"
    
    `#{spawnCommand}`
end

def SetupAssemblyInfo(asm)
    asm_version = BUILD_NUMBER

    if TEAMCITY
        puts "##teamcity[buildNumber '#{BUILD_NUMBER}']"
    end

    begin
        commit = `git log -1 --pretty=format:%H`
    rescue
        commit = "git unavailable"
    end

    testassemblies = FileList.new("#{TESTS_PATH}/*#{TEST_ASSEMBLY_PATTERN_PREFIX}.*/")
        .pathmap("%f")
        .collect! { |assemblyname|
            "[assembly: InternalsVisibleTo(\"#{ROOT_NAMESPACE}#{assemblyname}\")]"
        }

    asm.language = "C#"
    asm.version = BUILD_NUMBER
    asm.trademark = commit
    asm.file_version = BUILD_NUMBER
    asm.company_name = SOLUTION_COMPANY
    asm.product_name = SOLUTION_NAME
    asm.copyright = SOLUTION_COPYRIGHT
    asm.namespaces 'System.Runtime.CompilerServices'
    asm.custom_attributes :AssemblyConfiguration => CONFIG_TARGET, :AssemblyInformationalVersion => asm_version
    asm.custom_data testassemblies
    asm.output_file = ASSEMBLY_INFO
    asm.com_visible = false

    if WEB_ENABLED
        file = File.open(WEB_JS_VERSION, "w")

        begin
            file.write("var APEX_VERSION = \"#{BUILD_NUMBER}\";\n")
        rescue IOError => e
            p e
        ensure
            file.close unless file.nil?
        end
    end
end

def RunTests(boundary = "*", stop_on_fail = !TEAMCITY)
    runner = XUnitTestRunnerCustom.new(XUNIT_COMMAND)
    runner.html_output = RESULTS_PATH
    runner.skip_test_fail = !stop_on_fail
    
    assemblies = Array.new

    boundary.split(/,/).each do |this_boundary|
        FileList.new("#{TESTS_PATH}/*#{this_boundary}", "#{TESTS_PATH}/*#{this_boundary}.*")
                .collect! { |element| 
                    FileList.new("#{element}/**/*#{this_boundary}.dll", "#{element}/**/*#{this_boundary}.*.dll")
                        .exclude(/obj\//)
                        .each do |this_file|
                            if !this_file.include?('Data.') and ((!this_file.include?('Bus.Tests.') and !this_file.include?('Facade.Tests.') and !this_file.include?('Web.Command') and !this_file.include?('.Http.')))
                                assemblies.push this_file
                            end
                        end 
                }
    end

    if assemblies.length > 0
        runner.assemblies = assemblies
        runner.execute
    end

    return runner.error_count
end

desc "Run the Karma test runner once"
def RunKarma(configs, once = true, title = 'Karma Test', headless = false, forceCoverage = false, forceProgress = false)
    if !configs.nil?
        configs.each do |f|
            customReporters = ""
            if (TEAMCITY)
                customReporters = "teamcity"
            end
            if (forceProgress) 
                customReporters = ( customReporters.length == 0 ? "progress" : (customReporters + ",progress"))
            end
            if (forceCoverage)
                customReporters = ( customReporters.length == 0 ? "coverage" : (customReporters + ",coverage"))
            end

            karmaCommand = "start " + f + (once.nil? or once ? " --single-run" : "") + ((TEAMCITY or headless) ? " --browsers PhantomJS_custom" : "") + (TEAMCITY ? " --no-colors " : "") + ((customReporters.length > 0) ? ( " --reporters " + customReporters ) : "")
            if once
                if TEAMCITY
                    karmaResult = `#{KARMA_COMMAND} #{karmaCommand}`
                    puts karmaResult
                else
                    sh "#{KARMA_COMMAND} #{karmaCommand}"
                end
            else
                SpawnCommand(KARMA_COMMAND, karmaCommand, "#{BASE_PATH}/", title)
            end
        end
    end
end

def update_xml(xml_path)
    xml_file = File.new(xml_path)
    xml = REXML::Document.new xml_file
 
    yield xml
 
    xml_file.close
         
    xml_file = File.open(xml_path, "w")
    formatter = REXML::Formatters::Default.new(5)
    formatter.write(xml, xml_file)
    xml_file.close 
end

def RunUglify(rootDir)
    jsBundles = Dir.entries(rootDir).select {|entry| File.directory? File.join(rootDir,entry) }.sort()
    jsBundles.each { |bundleDirectory| 
        if ( bundleDirectory !='.' && bundleDirectory != '..')            
            fullDirectoryPath = File.join(rootDir,bundleDirectory)
            
            jsMatch = FileList.new("#{fullDirectoryPath}/*/*.js")
            
            if jsMatch.length == 0                
                RunUglify(fullDirectoryPath)
            else
                bundleName = File.basename(bundleDirectory)

                puts "Creating Bundle: #{bundleName}"
                
                bundleFiles = []

                fileCount = RecurseDirectoriesForUglify(fullDirectoryPath, bundleFiles)

                if (fileCount > 0)
                    allFiles = bundleFiles.join(" ")

                    bundleDepth = fullDirectoryPath.gsub(WEB_JS_PATH, '').sub(/^\//, '').split('/').length
                    bundleDirMask = rootDir.gsub("#{WEB_JS_PATH}", "").gsub(/^\//, "").gsub(/\/$/, "") 
                    bundleSubDir = "#{bundleDirMask}#{"/" unless bundleDirMask.empty?}"
                    bundlePath = "#{BUILD_JS_PATH}/#{bundleSubDir}#{bundleName}".gsub(/\/\//, "")
                    bundleDiagSourcePath = bundlePath.gsub(/\/app\//i, "/diagnostics/source/app/")
                    bundleDiagSourcemapsPaths = bundlePath.gsub(/\/app\//i, "/diagnostics/sourcemaps/")
                    bundledMinifiedFileName = "#{bundlePath}/#{bundleName}.min.js"
                    sourceMapMinifiedFileName = "#{BUILD_SOURCEMAPS_PATH}/#{bundleSubDir}#{bundleName}/#{bundleName}.min.js.map"
                    sourceMapMinifiedUrl = "#{("../"*bundleDepth)}diagnostics/sourcemaps/#{bundleSubDir}#{bundleName}/#{bundleName}.min.js.map"
                    sourceMapRoot = "../../#{("../"*bundleDepth)}diagnostics/source/"

                    CheckDirExists(BUILD_JS_PATH)
                    CheckDirExists(bundlePath)
                    CheckDirExists(bundleDiagSourcePath)
                    CheckDirExists(File.dirname(sourceMapMinifiedFileName))

                    sh "#{UGLIFY_COMMAND} #{allFiles} -m -c -o  \"#{bundledMinifiedFileName}\" --source-map \"#{sourceMapMinifiedFileName}\" --source-map-root #{sourceMapRoot} --source-map-url #{sourceMapMinifiedUrl} -p 11"

                    # After minification, insert the copyright message in it
                    file = File.open(bundledMinifiedFileName, "a")
                    begin
                        file.write("\n\n// #{SOLUTION_COPYRIGHT}\n\n")
                    rescue IOError => e
                        puts e
                    ensure
                        file.close unless file.nil?
                    end
                end
            end
        end
    }
end

def RecurseDirectoriesForUglify(directory, filesToBundle, fileCount = 0)
    # Go through sub directories first
    subDirs = Dir.entries(directory).select {|entry| File.directory? File.join(directory,entry) }.sort()
    subDirs.each { |subDirectory|
        if ( subDirectory !='.' && subDirectory != '..' && (!subDirectory.include?('test-data') || CONFIG_TARGET != "release"))
            puts " Processing subDirectory: #{subDirectory}"
            fullSubDirectoryPath = File.join(directory,subDirectory)
            fileCount = RecurseDirectoriesForUglify(fullSubDirectoryPath, filesToBundle, fileCount)
        end
    }

    Dir["#{directory}/*.js"].each { |fileName|
        puts " Bundle includes file: #{File.basename(fileName)}, copying source to diagnostics."

        # Need the 'app' part of the destination folder as uglify
        dest_filename = File.join(BUILD_SOURCEDIAG_PATH, fileName.gsub(WEB_JS_PATH, "app"))

        CheckDirExists File.dirname(dest_filename)

        FileUtils.cp(fileName, dest_filename)

        filesToBundle << fileName
        fileCount += 1
    }

    return fileCount
end

def CheckDirExists(directory)
    if !Dir.exists?(directory)
        iterative_path = ''

        directory.split(/\/|\\/).collect { |d|
            iterative_path = "#{iterative_path}#{'/' unless d.end_with?(':')}#{d}"
            Dir.mkdir(iterative_path) unless Dir.exists?(iterative_path)
        }
    end
end

# Albacore needs some Mono help
class XUnitTestRunnerCustom
    TaskName = :xunit
    include Albacore::Task
    include Albacore::RunCommand

    attr_accessor :html_output, :skip_test_fail, :error_count
    attr_array :options,:assembly,:assemblies

    def initialize(command=nil)
        @options=[]
        super()
        update_attributes Albacore.configuration.xunit.to_hash
        @command = command unless command.nil?
        @error_count = 0
    end

    def get_command_line
    command_params = []
    command_params << @command
    command_params << get_command_parameters
    commandline = command_params.join(" ")
    @logger.debug "Build XUnit Test Runner Command Line: " + commandline
    commandline
    end

    def get_command_parameters
    command_params = [] 
    command_params << @options.join(" ") unless @options.nil?
    command_params << build_html_output unless @html_output.nil?
    command_params
    end

    def execute()         
        @assemblies = [] if @assemblies.nil?
        @assemblies << @assembly unless @assembly.nil?
        fail_with_message 'At least one assembly is required for assemblies attr' if @assemblies.length==0  
        failure_message = 'XUnit Failed. See Build Log For Detail'      

        @assemblies.each do |assm|
          command_params = get_command_parameters.collect{ |p| p % File.basename(assm) }
          command_params.insert(0,assm) 
          result = run_command "XUnit", command_params.join(" ")
          @error_count = (@error_count + 1) if !result && $?.exitstatus > 1
          fail_with_message failure_message if !result && (!@skip_test_fail || $?.exitstatus > 1)
        end       
    end

    def build_html_output
        fail_with_message 'Directory is required for html_output' if !File.directory?(File.expand_path(@html_output))
        "/nunit \"@#{File.join(File.expand_path(@html_output),"%s.html")}\""
    end
end

class AssemblyInfoCustom < AssemblyInfo
  include Albacore::Task
  include Configuration::AssemblyInfo

  def build_using_statements(data)
    @namespaces = [] if @namespaces.nil?
    
    @namespaces << "System.Reflection"
    @namespaces << "System.Runtime.InteropServices"
    @namespaces.uniq!
    
    # This is the only change made to the default behaviour. It's so we can push out an auto-generated marker
    # at the top of the file, which gets StyleCop to ignore it.
    ns = ["// <auto-generated/>"]
    @namespaces.each do |n|
      ns << @lang_engine.build_using_statement(n) unless data.index { |l| l.match n }
    end
    
    ns
  end  
end
