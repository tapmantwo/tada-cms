PreRequisites
=============
Git – using msysgit available [here](https://code.google.com/p/msysgit/downloads/detail?name=Git-1.8.1.2-preview20130201.exe&can=2&q=) – NOTE if you have an older version please update!
Ruby > 1.9.3 available [here](http://rubyforge.org/frs/download.php/76798/rubyinstaller-1.9.3-p392.exe) – NOTE version 2 has been released but I haven’t tested it so suggest sticking with 1.9.3

After Installing Git
====================
After installing msysgit make sure your system path is updated with 
C:\Program Files (x86)\Git\cmd;C:\Program Files (x86)\Git\bin

Post-git
It’s recommend you use posh-git powershell prompt. If you haven’t got it installed then;

… from a Administrator PowerShell prompt:
Set-ExecutionPolicy RemoteSigned
“Y” – For “Yes”

.. then from a User PowerShell prompt:
(new-object Net.WebClient).DownloadString("http://psget.net/GetPsGet.ps1") | iex

Install-module posh-git